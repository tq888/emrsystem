package com.shynnness.admin.web;

import com.shynnness.admin.pojo.Admin;
import com.shynnness.admin.pojo.Code;
import com.shynnness.admin.pojo.LoginRequest;
import com.shynnness.admin.pojo.Result;
import com.shynnness.admin.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private IAdminService adminService;

    /**
     * 新增管理员
     *
     * @param admin
     * @return
     */
    @PostMapping
    public Result addAdmin(@RequestBody Admin admin) {
        boolean phoneNumberExists = adminService.checkPhoneNumberExists(admin.getPhoneNumber());
        if (phoneNumberExists) {
            return new Result(Code.ADD_ERROR, "该手机号码已被注册！");
        }
        boolean saved = adminService.save(admin);
        if (saved) {
            return new Result(Code.ADD_SUCCESS, null, "新增管理员成功！");
        } else {
            return new Result(Code.ADD_ERROR, null, "新增管理员失败！");
        }
    }

    /**
     * 删除管理员
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deleteAdmin(@PathVariable Long id) {
        Admin existingAdmin = adminService.getById(id);
        if (Objects.isNull(existingAdmin)) {
            return new Result(Code.DELETE_SUCCESS, null, "该管理员不存在！");
        }

        if (existingAdmin.getRole().equals(0)) {
            return new Result(Code.DELETE_ERROR, null, "该管理员为超级管理员，无法删除！");
        }

        boolean removed = adminService.removeById(id);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除管理员成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除管理员失败！");
        }
    }

    /**
     * 批量删除管理员
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/batch")
    public Result deleteAdmins(@RequestBody List<Long> ids) {
        if (ids.isEmpty()) {
            return new Result(Code.DELETE_ERROR, null, "批量删除管理员失败！");
        }
        for (Long id : ids) {
            Admin existingAdmin = adminService.getById(id);
            if (existingAdmin.getRole().equals(0)) {
                return new Result(Code.DELETE_ERROR, null, "删除的管理员中有超级管理员，无法删除！");
            }
        }
        boolean removed = adminService.removeByIds(ids);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除管理员成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除管理员失败！");
        }
    }

    /**
     * 更新管理员
     *
     * @param admin
     * @return
     */
    @PutMapping
    public Result updateAdmin(@RequestBody Admin admin) {
        Admin existingAdmin = adminService.getById(admin.getId());
        if (Objects.isNull(existingAdmin)) {
            return new Result(Code.DELETE_SUCCESS, null, "该管理员不存在！");
        }

        if (!existingAdmin.getPhoneNumber().equals(admin.getPhoneNumber())) {
            if (adminService.checkPhoneNumberExists(admin.getPhoneNumber())) {
                return new Result(Code.UPDATE_ERROR, null, "该手机号码已被注册！");
            }
        }

        boolean updated = adminService.updateById(admin);
        if (updated) {
            return new Result(Code.UPDATE_SUCCESS, null, "更新管理员信息成功！");
        } else {
            return new Result(Code.UPDATE_ERROR, null, "更新管理员信息失败！");
        }
    }

    /**
     * 根据ID查询管理员
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result getAdmin(@PathVariable Long id) {
        Admin admin = adminService.getById(id);
        if (Objects.nonNull(admin)) {
            return new Result(Code.SELECT_SUCCESS, admin, "查询管理员信息成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询管理员信息失败！");
        }
    }

    /**
     * 管理员登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/login")
    public Result login(@RequestBody LoginRequest loginRequest, HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        Admin admin = adminService.login(loginRequest);
        admin.setLastLoginIp(ip);
        boolean updated = adminService.updateById(admin);
        if (updated) {
            return new Result(Code.SELECT_SUCCESS, admin, "登录成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }
    }

    /**
     * 查询管理员
     *
     * @param admin
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/{current}/{orderType}/{field}")
    public Result searchAdmins(@RequestBody Admin admin,
                               @PathVariable("current") Integer current,
                               @PathVariable("orderType") String orderType,
                               @PathVariable("field") String field) {
        List<Admin> admins = adminService.searchAdmins(admin, current, orderType, field);
        if (null != admins && !admins.isEmpty()) {
            return new Result(Code.SELECT_SUCCESS, admins, "查询管理员信息成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询管理员信息失败！");
        }
    }

    /**
     * 统计管理员总数
     *
     * @param admin
     * @return
     */
    @PostMapping("/count")
    public Result countAdmins(@RequestBody Admin admin) {
        Integer total = adminService.countAdmins(admin);
        if (Objects.nonNull(total)) {
            return new Result(Code.SELECT_SUCCESS, total, "查询管理员总数成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询管理员总数失败！");
        }
    }
}
