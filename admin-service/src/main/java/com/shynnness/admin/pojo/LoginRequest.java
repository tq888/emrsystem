package com.shynnness.admin.pojo;

import lombok.Data;

@Data
public class LoginRequest {
    private String phoneNumber;
    private String password;
}
