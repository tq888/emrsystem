package com.shynnness.admin.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "Admin")
public class Admin {

    /**
     * 管理员ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 管理员姓名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 管理员密码
     */
    @TableField(value = "password")
    private String password;

    /**
     * 角色
     * （0为超级管理员、1为系统管理员、2为病历管理员、3为用户管理员、4为药品管理员）
     */
    @TableField(value = "role")
    private Integer role;

    /**
     * 联系电话
     */
    @TableField(value = "phoneNumber")
    private String phoneNumber;

    /**
     * 最后登录的ip地址
     */
    @TableField(value = "lastLoginIp")
    private String lastLoginIp;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    @TableField(value = "deleted")
    private Integer deleted;
}
