package com.shynnness.admin;

import com.shynnness.feign.client.*;
import com.shynnness.feign.config.DefaultFeignConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(clients = {
        AdminClient.class,
        AuthorizationClient.class,
        MedicalRecordClient.class,
        MedicalStaffClient.class,
        MedicationClient.class,
        PatientClient.class,
        PrescriptionClient.class
},
        defaultConfiguration = DefaultFeignConfiguration.class)
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}
