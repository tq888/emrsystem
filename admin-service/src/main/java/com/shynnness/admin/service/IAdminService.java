package com.shynnness.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shynnness.admin.pojo.Admin;
import com.shynnness.admin.pojo.LoginRequest;

import java.util.List;

public interface IAdminService extends IService<Admin> {

    /**
     * 检查电话是否存在
     *
     * @param phoneNumber
     * @return
     */
    boolean checkPhoneNumberExists(String phoneNumber);

    /**
     * 管理员登录
     *
     * @param loginRequest
     * @return
     */
    Admin login(LoginRequest loginRequest);

    /**
     * 查询管理员
     *
     * @param admin
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    List<Admin> searchAdmins(Admin admin, Integer current, String orderType, String field);

    /**
     * 统计管理员总数
     *
     * @param admin
     * @return
     */
    Integer countAdmins(Admin admin);
}
