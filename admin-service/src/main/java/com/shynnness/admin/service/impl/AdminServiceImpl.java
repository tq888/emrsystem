package com.shynnness.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shynnness.admin.mapper.AdminMapper;
import com.shynnness.admin.pojo.Admin;
import com.shynnness.admin.pojo.LoginRequest;
import com.shynnness.admin.service.IAdminService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {
    @Autowired
    private AdminMapper adminMapper;

    @Override
    public boolean checkPhoneNumberExists(String phoneNumber) {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phoneNumber", phoneNumber);
        return adminMapper.selectCount(queryWrapper) > 0;
    }

    @Override
    public Admin login(LoginRequest loginRequest) {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phoneNumber", loginRequest.getPhoneNumber());
        queryWrapper.eq("password", loginRequest.getPassword());
        return adminMapper.selectOne(queryWrapper);
    }

    @Override
    public List<Admin> searchAdmins(Admin admin, Integer current, String orderType, String field) {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(admin, orderType, field, queryWrapper);
        return adminMapper.selectPage(new Page<>(current, 10), queryWrapper).getRecords();
    }

    @Override
    public Integer countAdmins(Admin admin) {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(admin, null, null, queryWrapper);
        return Math.toIntExact(adminMapper.selectCount(queryWrapper));
    }

    private static void WrapperBuilder(Admin admin, String orderType, String field, QueryWrapper<Admin> queryWrapper) {
        if (Objects.nonNull(admin.getId())) {
            queryWrapper.eq("id", admin.getId());
        }
        if (StringUtils.isNotEmpty(admin.getName())) {
            queryWrapper.like("name", admin.getName());
        }
        if (Objects.nonNull(admin.getRole())) {
            queryWrapper.eq("role", admin.getRole());
        }
        if (StringUtils.isNotEmpty(admin.getPhoneNumber())) {
            queryWrapper.like("phoneNumber", admin.getPhoneNumber());
        }
        if (StringUtils.isNotEmpty(admin.getLastLoginIp())) {
            queryWrapper.like("lastLoginIp", admin.getLastLoginIp());
        }
        if (StringUtils.isNotEmpty(orderType) && StringUtils.isNotEmpty(field)) {
            if ("asc".equals(orderType)) {
                queryWrapper.orderByAsc(field);
            } else if ("desc".equals(orderType)) {
                queryWrapper.orderByDesc(field);
            }
        }
    }
}
