package com.shynnness.feign.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "NursingRecord")
public class NursingRecord {

    /**
     * 记录ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 患者ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "patientId")
    private Long patientId;

    /**
     * 护士ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "nurseId")
    private Long nurseId;

    /**
     * 科室
     */
    @TableField(value = "department")
    private String department;

    /**
     * 记录时间
     */
    @TableField(value = "recordTime")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime recordTime;

    /**
     * 护理笔记
     */
    @TableField(value = "notes")
    private String notes;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    @TableField(value = "deleted")
    private Integer deleted;

    @TableField(exist = false)
    private Object patient;
    @TableField(exist = false)
    private Object nurse;

    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
    private LocalDateTime startRecordTime;
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
    private LocalDateTime endRecordTime;
}
