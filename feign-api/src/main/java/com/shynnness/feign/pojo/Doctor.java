package com.shynnness.feign.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "Doctor")
public class Doctor {

    /**
     * 医生ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 医生姓名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 医生密码
     */
    @TableField(value = "password")
    private String password;

    /**
     * 专业
     */
    @TableField(value = "specialization")
    private String specialization;

    /**
     * 科室
     */
    @TableField(value = "department")
    private String department;

    /**
     * 联系电话
     */
    @TableField(value = "phoneNumber")
    private String phoneNumber;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    @TableField(value = "deleted")
    private Integer deleted;
}
