package com.shynnness.feign.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "MedicalRecord")
public class MedicalRecord {

    /**
     * 记录ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 患者ID
     */
    @TableField(value = "patientId")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long patientId;

    /**
     * 访问日期
     */
    @TableField(value = "visitDate")
    private LocalDateTime visitDate;

    /**
     * 科室
     */
    @TableField(value = "department")
    private String department;

    /**
     * 症状
     */
    @TableField(value = "symptoms")
    private String symptoms;

    /**
     * 诊断
     */
    @TableField(value = "diagnosis")
    private String diagnosis;

    /**
     * 治疗计划
     */
    @TableField(value = "treatmentPlan")
    private String treatmentPlan;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    @TableField(value = "deleted")
    private Integer deleted;

    @TableField(exist = false)
    private Object patient;

    @TableField(exist = false)
    private LocalDateTime startVisitDate;
    @TableField(exist = false)
    private LocalDateTime endVisitDate;
}
