package com.shynnness.feign.client;

import com.shynnness.feign.pojo.Medication;
import com.shynnness.feign.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "http://localhost:8086")
public interface MedicationClient {

    /**
     * 新增药物
     *
     * @param medication
     * @return
     */
    @PostMapping("/medication")
    Result addMedication(@RequestBody Medication medication);

    /**
     * 删除药物
     *
     * @param id
     * @return
     */
    @DeleteMapping("/medication/{id}")
    Result deleteMedication(@PathVariable Long id);

    /**
     * 批量删除药物
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/medication/batch")
    Result deleteMedications(@RequestBody List<Long> ids);

    /**
     * 更新药物
     *
     * @param medication
     * @return
     */
    @PutMapping("/medication")
    Result updateMedication(@RequestBody Medication medication);

    /**
     * 根据ID查询药物
     *
     * @param id
     * @return
     */
    @GetMapping("/medication/{id}")
    Result getMedication(@PathVariable Long id);

    /**
     * 查询所有药物
     *
     * @return
     */
    @GetMapping("/medication")
    Result getAllMedications();

    /**
     * 查询药物
     *
     * @param medication
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/medication/{current}/{orderType}/{field}")
    Result searchMedications(@RequestBody Medication medication,
                             @PathVariable("current") Integer current,
                             @PathVariable("orderType") String orderType,
                             @PathVariable("field") String field);

    /**
     * 统计药物总数
     *
     * @param medication
     * @return
     */
    @PostMapping("/medication/count")
    Result countMedications(@RequestBody Medication medication);
}
