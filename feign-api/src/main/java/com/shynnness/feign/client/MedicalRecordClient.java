package com.shynnness.feign.client;

import com.shynnness.feign.pojo.MedicalRecord;
import com.shynnness.feign.pojo.NursingRecord;
import com.shynnness.feign.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "http://localhost:8084")
public interface MedicalRecordClient {

    /**
     * 新增医疗记录
     *
     * @param medicalRecord
     * @return
     */
    @PostMapping("/medicalRecord")
    Result addMedicalRecord(@RequestBody MedicalRecord medicalRecord);

    /**
     * 删除医疗记录
     *
     * @param id
     * @return
     */
    @DeleteMapping("/medicalRecord/{id}")
    Result deleteMedicalRecord(@PathVariable Long id);

    /**
     * 修改医疗记录
     *
     * @param medicalRecord
     * @return
     */
    @PutMapping("/medicalRecord")
    Result updateMedicalRecord(@RequestBody MedicalRecord medicalRecord);

    /**
     * 根据ID查询医疗记录
     *
     * @param id
     * @return
     */
    @GetMapping("/medicalRecord/{id}")
    Result getMedicalRecord(@PathVariable Long id);

    /**
     * 查询医疗记录
     *
     * @param medicalRecord
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/medicalRecord/{current}/{orderType}/{field}")
    Result searchMedicalRecords(@RequestBody MedicalRecord medicalRecord,
                                @PathVariable("current") Integer current,
                                @PathVariable("orderType") String orderType,
                                @PathVariable("field") String field);

    /**
     * 统计医疗记录总数
     *
     * @param medicalRecord
     * @return
     */
    @PostMapping("/medicalRecord/count")
    Result countMedicalRecords(@RequestBody MedicalRecord medicalRecord);

    /**
     * 新增护理记录
     *
     * @param nursingRecord
     * @return
     */
    @PostMapping("/nursingRecord")
    Result addNursingRecord(@RequestBody NursingRecord nursingRecord);

    /**
     * 删除护理记录
     *
     * @param id
     * @return
     */
    @DeleteMapping("/nursingRecord/{id}")
    Result deleteNursingRecord(@PathVariable Long id);

    /**
     * 修改护理记录
     *
     * @param nursingRecord
     * @return
     */
    @PutMapping("/nursingRecord")
    Result updateNursingRecord(@RequestBody NursingRecord nursingRecord);

    /**
     * 根据ID查询护理记录
     *
     * @param id
     * @return
     */
    @GetMapping("/nursingRecord/{id}")
    Result getNursingRecord(@PathVariable Long id);

    /**
     * 查询护理记录
     *
     * @param nursingRecord
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/nursingRecord/{current}/{orderType}/{field}")
    Result searchNursingRecords(@RequestBody NursingRecord nursingRecord,
                                @PathVariable("current") Integer current,
                                @PathVariable("orderType") String orderType,
                                @PathVariable("field") String field);

    /**
     * 统计医疗记录总数
     *
     * @param nursingRecord
     * @return
     */
    @PostMapping("/nursingRecord/count")
    Result countNursingRecords(@RequestBody NursingRecord nursingRecord);

}
