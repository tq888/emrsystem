package com.shynnness.feign.client;

import com.shynnness.feign.pojo.LoginRequest;
import com.shynnness.feign.pojo.Patient;
import com.shynnness.feign.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "http://localhost:8081")
public interface PatientClient {
    /**
     * 新增患者
     *
     * @param patient
     * @return
     */
    @PostMapping("/patient")
    Result addPatient(@RequestBody Patient patient);

    /**
     * 删除患者
     *
     * @param id
     * @return
     */
    @DeleteMapping("/patient/{id}")
    Result deletePatient(@PathVariable Long id);

    /**
     * 批量删除患者
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/patient/batch")
    Result deletePatients(@RequestBody List<Long> ids);

    /**
     * 更新患者
     *
     * @param patient
     * @return
     */
    @PutMapping("/patient")
    Result updatePatient(@RequestBody Patient patient);

    /**
     * 根据ID查询患者
     *
     * @param id
     * @return
     */
    @GetMapping("/patient/{id}")
    Result getPatient(@PathVariable Long id);

    /**
     * 患者登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/patient/login")
    Result login(@RequestBody LoginRequest loginRequest);

    /**
     * 查询患者
     *
     * @param patient
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/patient/{current}/{orderType}/{field}")
    Result searchPatients(@RequestBody Patient patient,
                          @PathVariable("current") Integer current,
                          @PathVariable("orderType") String orderType,
                          @PathVariable("field") String field);

    /**
     * 统计患者总数
     *
     * @param patient
     * @return
     */
    @PostMapping("/patient/count")
    Result countPatients(@RequestBody Patient patient);
}
