package com.shynnness.feign.client;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "http://localhost:8087")
public interface AuthorizationClient {
}
