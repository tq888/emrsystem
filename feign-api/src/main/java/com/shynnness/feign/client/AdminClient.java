package com.shynnness.feign.client;

import com.shynnness.feign.pojo.Admin;
import com.shynnness.feign.pojo.LoginRequest;
import com.shynnness.feign.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "http://localhost:8083")
public interface AdminClient {

    /**
     * 新增管理员
     *
     * @param admin
     * @return
     */
    @PostMapping("/admin")
    Result addAdmin(@RequestBody Admin admin);

    /**
     * 删除管理员
     *
     * @param id
     * @return
     */
    @DeleteMapping("/admin/{id}")
    Result deleteAdmin(@PathVariable Long id);

    /**
     * 批量删除管理员
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/admin/batch")
    Result deleteAdmins(@RequestBody List<Long> ids);

    /**
     * 更新管理员
     *
     * @param admin
     * @return
     */
    @PutMapping("/admin")
    Result updateAdmin(@RequestBody Admin admin);

    /**
     * 根据ID查询管理员
     *
     * @param id
     * @return
     */
    @GetMapping("/admin/{id}")
    Result getAdmin(@PathVariable Long id);

    /**
     * 管理员登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/admin/login")
    Result login(@RequestBody LoginRequest loginRequest);

    /**
     * 查询管理员
     *
     * @param admin
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/admin/{current}/{orderType}/{field}")
    Result searchAdmins(@RequestBody Admin admin,
                        @PathVariable("current") Integer current,
                        @PathVariable("orderType") String orderType,
                        @PathVariable("field") String field);

    /**
     * 统计管理员总数
     *
     * @param admin
     * @return
     */
    @PostMapping("/admin/count")
    Result countAdmins(@RequestBody Admin admin);
}
