package com.shynnness.feign.client;

import com.shynnness.feign.pojo.Doctor;
import com.shynnness.feign.pojo.LoginRequest;
import com.shynnness.feign.pojo.Nurse;
import com.shynnness.feign.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "http://localhost:8082")
public interface MedicalStaffClient {
    /**
     * 新增医生
     *
     * @param doctor
     * @return
     */
    @PostMapping("/doctor")
    Result addDoctor(@RequestBody Doctor doctor);

    /**
     * 删除医生
     *
     * @param id
     * @return
     */
    @DeleteMapping("/doctor/{id}")
    Result deleteDoctor(@PathVariable Long id);

    /**
     * 批量删除医生
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/doctor/batch")
    Result deleteDoctors(@RequestBody List<Long> ids);

    /**
     * 更新医生
     *
     * @param doctor
     * @return
     */
    @PutMapping("/doctor")
    Result updateDoctor(@RequestBody Doctor doctor);

    /**
     * 根据ID查询医生
     *
     * @param id
     * @return
     */
    @GetMapping("/doctor/{id}")
    Result getDoctor(@PathVariable Long id);

    /**
     * 医生登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/doctor/login")
    Result doctorLogin(@RequestBody LoginRequest loginRequest);

    /**
     * 查询医生
     *
     * @param doctor
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/doctor/{current}/{orderType}/{field}")
    Result searchDoctors(@RequestBody Doctor doctor,
                         @PathVariable("current") Integer current,
                         @PathVariable("orderType") String orderType,
                         @PathVariable("field") String field);

    /**
     * 统计医生总数
     *
     * @param doctor
     * @return
     */
    @PostMapping("/doctor/count")
    Result countDoctors(@RequestBody Doctor doctor);

    /**
     * 新增护士
     *
     * @param nurse
     * @return
     */
    @PostMapping("/nurse")
    Result addNurse(@RequestBody Nurse nurse);

    /**
     * 删除护士
     *
     * @param id
     * @return
     */
    @DeleteMapping("/nurse/{id}")
    Result deleteNurse(@PathVariable Long id);

    /**
     * 批量删除医生
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/nurse/batch")
    Result deleteNurses(@RequestBody List<Long> ids);

    /**
     * 更新护士
     *
     * @param nurse
     * @return
     */
    @PutMapping("/nurse")
    Result updateNurse(@RequestBody Nurse nurse);

    /**
     * 根据ID获取护士
     *
     * @param id
     * @return
     */
    @GetMapping("/nurse/{id}")
    Result getNurse(@PathVariable Long id);

    /**
     * 护士登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/nurse/login")
    Result nurseLogin(@RequestBody LoginRequest loginRequest);

    /**
     * 查询护士
     *
     * @param nurse
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/nurse/{current}/{orderType}/{field}")
    Result searchNurses(@RequestBody Nurse nurse,
                        @PathVariable("current") Integer current,
                        @PathVariable("orderType") String orderType,
                        @PathVariable("field") String field);
    /**
     * 统计医生总数
     *
     * @param nurse
     * @return
     */
    @PostMapping("/nurse/count")
    Result countNurses(@RequestBody Nurse nurse);
}
