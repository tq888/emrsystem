package com.shynnness.feign.client;

import com.shynnness.feign.pojo.Prescription;
import com.shynnness.feign.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "http://localhost:8085")
public interface PrescriptionClient {

    /**
     * 新增处方
     *
     * @param prescription
     * @return
     */
    @PostMapping("/prescription")
    Result addPrescription(@RequestBody Prescription prescription);

    /**
     * 删除处方
     *
     * @param id
     * @return
     */
    @DeleteMapping("/prescription/{id}")
    Result deletePrescription(@PathVariable Long id);

    /**
     * 更新处方
     *
     * @param prescription
     * @return
     */
    @PutMapping("/prescription")
    Result updatePrescription(@RequestBody Prescription prescription);

    /**
     * 根据医疗记录ID删除处方
     *
     * @param medicalRecordId
     * @return
     */
    @DeleteMapping("/prescription/medicalRecordId/{medicalRecordId}")
    Result deleteByMedicalRecordId(@PathVariable Long medicalRecordId);

    /**
     * 根据医疗记录ID查询处方
     *
     * @param medicalRecordId
     * @return
     */
    @GetMapping("/prescription/medicalRecord/{medicalRecordId}")
    Result getByMedicalRecordId(@PathVariable Long medicalRecordId);

    /**
     * 根据ID查询处方
     *
     * @param id
     * @return
     */
    @GetMapping("/prescription/{id}")
    Result getPrescription(@PathVariable Long id);

    /**
     * 查询处方
     *
     * @param prescription
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/prescription/{current}/{orderType}/{field}")
    Result searchPrescriptions(@RequestBody Prescription prescription,
                               @PathVariable("current") Integer current,
                               @PathVariable("orderType") String orderType,
                               @PathVariable("field") String field);

    /**
     * 统计处方总数
     *
     * @param prescription
     * @return
     */
    @PostMapping("/prescription/count")
    Result countPrescriptions(@RequestBody Prescription prescription);

    /**
     * 审核处方
     *
     * @param id
     * @param status
     * @return
     */
    @PutMapping("/prescription/review/{id}/{status}")
    Result reviewPrescription(@PathVariable Long id, @PathVariable Integer status);
}
