package com.shynnness.authorization.controller;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.shynnness.authorization.pojo.Code;
import com.shynnness.authorization.util.JWTUtil;
import com.shynnness.feign.client.MedicalStaffClient;
import com.shynnness.feign.client.PatientClient;
import com.shynnness.feign.pojo.LoginRequest;
import com.shynnness.authorization.pojo.Result;
import com.shynnness.feign.client.AdminClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/authorization")
public class AuthorizationController {

    @Autowired
    private AdminClient adminClient;
    @Autowired
    private MedicalStaffClient medicalStaffClient;
    @Autowired
    private PatientClient patientClient;

    /**
     * 管理员登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/admin/login")
    public Result adminLogin(@RequestBody LoginRequest loginRequest) {
        com.shynnness.feign.pojo.Result loginResult = adminClient.login(loginRequest);
        if (20040 == loginResult.getCode()) {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }

        try {
            Map<String, Object> data = (Map<String, Object>) loginResult.getData();
            HashMap<String, String> adminInfo = new HashMap<>();
            adminInfo.put("id", data.get("id").toString());
            adminInfo.put("name", data.get("name").toString());
            adminInfo.put("adminRole", data.get("role").toString());
            adminInfo.put("phoneNumber", data.get("phoneNumber").toString());

            adminInfo.put("role", "admin");

            String token = JWTUtil.getAdminToken(adminInfo);
            return new Result(Code.SELECT_SUCCESS, token, "登录成功！");
        } catch (Exception e) {
            return new Result(Code.SELECT_ERROR, null, "登录失败！");
        }
    }

    /**
     * 医生登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/doctor/login")
    public Result doctorLogin(@RequestBody LoginRequest loginRequest) {
        com.shynnness.feign.pojo.Result loginResult = medicalStaffClient.doctorLogin(loginRequest);
        if (20040 == loginResult.getCode()) {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }

        try {
            Map<String, Object> data = (Map<String, Object>) loginResult.getData();
            HashMap<String, String> doctorInfo = new HashMap<>();
            doctorInfo.put("id", data.get("id").toString());
            doctorInfo.put("name", data.get("name").toString());
            doctorInfo.put("phoneNumber", data.get("phoneNumber").toString());

            doctorInfo.put("role", "doctor");

            String token = JWTUtil.getAdminToken(doctorInfo);
            return new Result(Code.SELECT_SUCCESS, token, "登录成功！");
        } catch (Exception e) {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }
    }

    /**
     * 护士登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/nurse/login")
    public Result nurseLogin(@RequestBody LoginRequest loginRequest) {
        com.shynnness.feign.pojo.Result loginResult = medicalStaffClient.nurseLogin(loginRequest);
        if (20040 == loginResult.getCode()) {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }

        try {
            Map<String, Object> data = (Map<String, Object>) loginResult.getData();
            HashMap<String, String> nurseInfo = new HashMap<>();
            nurseInfo.put("id", data.get("id").toString());
            nurseInfo.put("name", data.get("name").toString());
            nurseInfo.put("phoneNumber", data.get("phoneNumber").toString());

            nurseInfo.put("role", "nurse");

            String token = JWTUtil.getAdminToken(nurseInfo);
            return new Result(Code.SELECT_SUCCESS, token, "登录成功！");
        } catch (Exception e) {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }
    }

    /**
     * 患者登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/patient/login")
    public Result patientLogin(@RequestBody LoginRequest loginRequest) {
        com.shynnness.feign.pojo.Result loginResult = patientClient.login(loginRequest);
        if (20040 == loginResult.getCode()) {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }

        try {
            Map<String, Object> data = (Map<String, Object>) loginResult.getData();
            HashMap<String, String> patientInfo = new HashMap<>();
            patientInfo.put("id", data.get("id").toString());
            patientInfo.put("name", data.get("name").toString());
            patientInfo.put("phoneNumber", data.get("phoneNumber").toString());

            patientInfo.put("role", "patient");

            String token = JWTUtil.getUserToken(patientInfo);
            return new Result(Code.SELECT_SUCCESS, token, "登录成功！");
        } catch (Exception e) {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }
    }

    /**
     * 查询管理员信息
     *
     * @param request
     * @return
     */
    @GetMapping("/admin/info")
    public Result getAdminInfo(HttpServletRequest request) {
        String token = request.getHeader("token");
        DecodedJWT verify = JWTUtil.verify(token, true);

        String id = verify.getClaim("id").asString();
        String name = verify.getClaim("name").asString();
        String adminRole = verify.getClaim("adminRole").asString();
        String phoneNumber = verify.getClaim("phoneNumber").asString();
        String role = verify.getClaim("role").asString();

        HashMap<String, String> info = new HashMap<>();
        info.put("id", id);
        info.put("name", name);
        info.put("adminRole", adminRole);
        info.put("phoneNumber", phoneNumber);
        info.put("role", role);

        return new Result(Code.SELECT_SUCCESS, info, "查询成功！");
    }

    /**
     * 查询用户信息
     *
     * @param request
     * @return
     */
    @GetMapping("/user/info")
    public Result getUserInfo(HttpServletRequest request) {
        String token = request.getHeader("token");
        DecodedJWT verify = JWTUtil.verify(token, false);

        String id = verify.getClaim("id").asString();
        String name = verify.getClaim("name").asString();
        String phoneNumber = verify.getClaim("phoneNumber").asString();
        String role = verify.getClaim("role").asString();

        HashMap<String, String> info = new HashMap<>();
        info.put("id", id);
        info.put("name", name);
        info.put("phoneNumber", phoneNumber);
        info.put("role", role);

        return new Result(Code.SELECT_SUCCESS, info, "查询成功！");
    }
}
