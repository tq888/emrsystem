package com.shynnness.authorization.pojo;

import lombok.Data;

@Data
public class LoginRequest {
    private String phoneNumber;
    private String password;
}
