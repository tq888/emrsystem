package com.shynnness.medicalRecord.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shynnness.medicalRecord.mapper.MedicalRecordMapper;
import com.shynnness.medicalRecord.pojo.MedicalRecord;
import com.shynnness.medicalRecord.service.IMedicalRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class MedicalRecordServiceImpl extends ServiceImpl<MedicalRecordMapper, MedicalRecord> implements IMedicalRecordService {
    @Autowired
    private MedicalRecordMapper medicalRecordMapper;

    @Override
    public List<MedicalRecord> searchMedicalRecords(MedicalRecord medicalRecord, Integer current, String orderType, String field) {
        QueryWrapper<MedicalRecord> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(medicalRecord, orderType, field, queryWrapper);
        return medicalRecordMapper.selectPage(new Page<>(current, 10), queryWrapper).getRecords();
    }

    @Override
    public Integer countMedicalRecords(MedicalRecord medicalRecord) {
        QueryWrapper<MedicalRecord> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(medicalRecord, null, null, queryWrapper);
        return Math.toIntExact(medicalRecordMapper.selectCount(queryWrapper));
    }

    private static void WrapperBuilder(MedicalRecord medicalRecord, String orderType, String field, QueryWrapper<MedicalRecord> queryWrapper) {
        if (Objects.nonNull(medicalRecord.getId())) {
            queryWrapper.eq("id", medicalRecord.getId());
        }
        if (Objects.nonNull(medicalRecord.getPatientId())) {
            queryWrapper.eq("patientId", medicalRecord.getPatientId());
        }
        if (Objects.nonNull(medicalRecord.getStartVisitDate())) {
            queryWrapper.ge("visitDate", medicalRecord.getStartVisitDate());
        }
        if (Objects.nonNull(medicalRecord.getEndVisitDate())) {
            queryWrapper.le("visitDate", medicalRecord.getEndVisitDate());
        }
        if (StringUtils.isNotEmpty(medicalRecord.getDepartment())){
            queryWrapper.eq("department", medicalRecord.getDepartment());
        }
        if (StringUtils.isNotEmpty(medicalRecord.getSymptoms())) {
            queryWrapper.like("symptoms", medicalRecord.getSymptoms());
        }
        if (StringUtils.isNotEmpty(medicalRecord.getDiagnosis())) {
            queryWrapper.like("diagnosis", medicalRecord.getDiagnosis());
        }
        if (StringUtils.isNotEmpty(medicalRecord.getTreatmentPlan())) {
            queryWrapper.like("treatmentPlan", medicalRecord.getTreatmentPlan());
        }
        if (StringUtils.isNotEmpty(orderType) && StringUtils.isNotEmpty(field)) {
            if ("asc".equals(orderType)) {
                queryWrapper.orderByAsc(field);
            } else if ("desc".equals(orderType)) {
                queryWrapper.orderByDesc(field);
            }
        }
    }
}
