package com.shynnness.medicalRecord.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shynnness.medicalRecord.pojo.MedicalRecord;

import java.util.List;

public interface IMedicalRecordService extends IService<MedicalRecord> {

    /**
     * 查询医疗记录
     *
     * @param medicalRecord
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    List<MedicalRecord> searchMedicalRecords(MedicalRecord medicalRecord, Integer current, String orderType, String field);

    /**
     * 统计医疗记录总数
     *
     * @param medicalRecord
     * @return
     */
    Integer countMedicalRecords(MedicalRecord medicalRecord);
}
