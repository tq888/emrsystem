package com.shynnness.medicalRecord.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shynnness.medicalRecord.mapper.NursingRecordMapper;
import com.shynnness.medicalRecord.pojo.NursingRecord;
import com.shynnness.medicalRecord.service.INursingRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class NursingRecordServiceImpl extends ServiceImpl<NursingRecordMapper, NursingRecord> implements INursingRecordService {
    @Autowired
    private NursingRecordMapper nursingRecordMapper;

    @Override
    public List<NursingRecord> searchNursingRecords(NursingRecord nursingRecord, Integer current, String orderType, String field) {
        QueryWrapper<NursingRecord> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(nursingRecord, orderType, field, queryWrapper);
        return nursingRecordMapper.selectPage(new Page<>(current, 10), queryWrapper).getRecords();
    }

    @Override
    public Integer countNursingRecords(NursingRecord nursingRecord) {
        QueryWrapper<NursingRecord> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(nursingRecord, null, null, queryWrapper);
        return Math.toIntExact(nursingRecordMapper.selectCount(queryWrapper));
    }

    private static void WrapperBuilder(NursingRecord nursingRecord, String orderType, String field, QueryWrapper<NursingRecord> queryWrapper) {
        if (Objects.nonNull(nursingRecord.getId())) {
            queryWrapper.eq("id", nursingRecord.getId());
        }
        if (Objects.nonNull(nursingRecord.getPatientId())) {
            queryWrapper.eq("patientId", nursingRecord.getPatientId());
        }
        if (Objects.nonNull(nursingRecord.getNurseId())) {
            queryWrapper.eq("nurseId", nursingRecord.getNurseId());
        }
        if (StringUtils.isNotEmpty(nursingRecord.getDepartment())){
            queryWrapper.eq("department", nursingRecord.getDepartment());
        }
        if (Objects.nonNull(nursingRecord.getStartRecordTime())) {
            queryWrapper.ge("recordTime", nursingRecord.getStartRecordTime());
        }
        if (Objects.nonNull(nursingRecord.getEndRecordTime())) {
            queryWrapper.le("recordTime", nursingRecord.getEndRecordTime());
        }
        if (StringUtils.isNotEmpty(nursingRecord.getNotes())) {
            queryWrapper.like("notes", nursingRecord.getNotes());
        }
        if (StringUtils.isNotEmpty(orderType) && StringUtils.isNotEmpty(field)) {
            if ("asc".equals(orderType)) {
                queryWrapper.orderByAsc(field);
            } else if ("desc".equals(orderType)) {
                queryWrapper.orderByDesc(field);
            }
        }
    }
}
