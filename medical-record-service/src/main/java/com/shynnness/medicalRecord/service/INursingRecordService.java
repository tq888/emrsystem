package com.shynnness.medicalRecord.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shynnness.medicalRecord.pojo.NursingRecord;

import java.util.List;

public interface INursingRecordService extends IService<NursingRecord> {

    /**
     * 查询护理记录
     *
     * @param nursingRecord
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    List<NursingRecord> searchNursingRecords(NursingRecord nursingRecord, Integer current, String orderType, String field);

    /**
     * 统计护理记录总数
     *
     * @param nursingRecord
     * @return
     */
    Integer countNursingRecords(NursingRecord nursingRecord);
}
