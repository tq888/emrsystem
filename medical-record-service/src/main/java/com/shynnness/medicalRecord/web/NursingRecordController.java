package com.shynnness.medicalRecord.web;

import com.shynnness.feign.client.MedicalStaffClient;
import com.shynnness.feign.client.PatientClient;
import com.shynnness.medicalRecord.pojo.Code;
import com.shynnness.medicalRecord.pojo.NursingRecord;
import com.shynnness.medicalRecord.pojo.Result;
import com.shynnness.medicalRecord.service.INursingRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/nursingRecord")
public class NursingRecordController {
    @Autowired
    private INursingRecordService nursingRecordService;
    @Autowired
    private PatientClient patientClient;
    @Autowired
    private MedicalStaffClient medicalStaffClient;

    /**
     * 新增护理记录
     *
     * @param nursingRecord
     * @return
     */
    @PostMapping
    public Result addNursingRecord(@RequestBody NursingRecord nursingRecord) {
        com.shynnness.feign.pojo.Result patientResult = patientClient.getPatient(nursingRecord.getPatientId());
        if (patientResult.getCode()==20040){
            return new Result(Code.ADD_ERROR, null, "新增护理记录失败！");
        }
        Object patient = patientResult.getData();
        if (Objects.isNull(patient)){
            return new Result(Code.ADD_ERROR, null, "该患者不存在！");
        }

        nursingRecord.setRecordTime(LocalDateTime.now());
        boolean saved = nursingRecordService.save(nursingRecord);
        if (saved) {
            return new Result(Code.ADD_SUCCESS, null, "新增护理记录成功！");
        } else {
            return new Result(Code.ADD_ERROR, null, "新增护理记录失败！");
        }
    }

    /**
     * 删除护理记录
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deleteNursingRecord(@PathVariable Long id) {
        boolean removed = nursingRecordService.removeById(id);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除护理记录成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除护理记录失败！");
        }
    }

    /**
     * 修改护理记录
     *
     * @param nursingRecord
     * @return
     */
    @PutMapping
    public Result updateNursingRecord(@RequestBody NursingRecord nursingRecord) {
        boolean updated = nursingRecordService.updateById(nursingRecord);
        if (updated) {
            return new Result(Code.UPDATE_SUCCESS, null, "更新护理记录成功！");
        } else {
            return new Result(Code.UPDATE_ERROR, null, "更新护理记录失败！");
        }
    }

    /**
     * 根据ID查询护理记录
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result getNursingRecord(@PathVariable Long id) {
        NursingRecord nursingRecord = nursingRecordService.getById(id);

        com.shynnness.feign.pojo.Result patientResult = patientClient.getPatient(nursingRecord.getPatientId());
        if (patientResult.getCode() == 20040) {
            return new Result(Code.SELECT_ERROR, null, "查询护理记录失败！");
        }
        nursingRecord.setPatient(patientResult.getData());

        com.shynnness.feign.pojo.Result nurseResult = medicalStaffClient.getNurse(nursingRecord.getNurseId());
        if (patientResult.getCode() == 20040) {
            return new Result(Code.SELECT_ERROR, null, "查询护理记录失败！");
        }
        nursingRecord.setNurse(nurseResult.getData());

        if (Objects.nonNull(nursingRecord)) {
            return new Result(Code.SELECT_SUCCESS, nursingRecord, "查询护理记录成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询护理记录失败！");
        }
    }

    /**
     * 查询护理记录
     *
     * @param nursingRecord
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/{current}/{orderType}/{field}")
    public Result searchNursingRecords(@RequestBody NursingRecord nursingRecord,
                                       @PathVariable("current") Integer current,
                                       @PathVariable("orderType") String orderType,
                                       @PathVariable("field") String field) {
        List<NursingRecord> nursingRecords = nursingRecordService.searchNursingRecords(nursingRecord, current, orderType, field);
        System.out.println(nursingRecords);
        if (null != nursingRecords && !nursingRecords.isEmpty()) {
            for (NursingRecord _nursingRecord : nursingRecords) {
                com.shynnness.feign.pojo.Result patientResult = patientClient.getPatient(_nursingRecord.getPatientId());
                if (patientResult.getCode() == 20040) {
                    return new Result(Code.SELECT_ERROR, null, "查询护理记录失败！");
                }
                _nursingRecord.setPatient(patientResult.getData());

                com.shynnness.feign.pojo.Result nurseResult = medicalStaffClient.getNurse(_nursingRecord.getNurseId());
                if (patientResult.getCode() == 20040) {
                    return new Result(Code.SELECT_ERROR, null, "查询护理记录失败！");
                }
                _nursingRecord.setNurse(nurseResult.getData());
            }
            return new Result(Code.SELECT_SUCCESS, nursingRecords, "查询护理记录成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询护理记录失败！");
        }
    }

    /**
     * 统计护理记录总数
     *
     * @param nursingRecord
     * @return
     */
    @PostMapping("/count")
    public Result countNursingRecords(@RequestBody NursingRecord nursingRecord) {
        Integer total = nursingRecordService.countNursingRecords(nursingRecord);
        if (Objects.nonNull(total)) {
            return new Result(Code.SELECT_SUCCESS, total, "查询护理记录总数成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询护理记录总数失败！");
        }
    }
}
