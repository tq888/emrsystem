package com.shynnness.medicalRecord.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.shynnness.feign.client.PatientClient;
import com.shynnness.feign.client.PrescriptionClient;
import com.shynnness.feign.pojo.Patient;
import com.shynnness.feign.pojo.Prescription;
import com.shynnness.medicalRecord.pojo.Code;
import com.shynnness.medicalRecord.pojo.MedicalRecord;
import com.shynnness.medicalRecord.pojo.Result;
import com.shynnness.medicalRecord.service.IMedicalRecordService;
import net.sf.jsqlparser.expression.StringValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import com.itextpdf.kernel.pdf.PdfDocument;

@RestController
@RequestMapping("/medicalRecord")
public class MedicalRecordController {
    @Autowired
    private IMedicalRecordService medicalRecordService;
    @Autowired
    private PrescriptionClient prescriptionClient;
    @Autowired
    private PatientClient patientClient;

    /**
     * 新增医疗记录
     *
     * @param medicalRecord
     * @return
     */
    @PostMapping
    public Result addMedicalRecord(@RequestBody MedicalRecord medicalRecord) {
        com.shynnness.feign.pojo.Result patientResult = patientClient.getPatient(medicalRecord.getPatientId());
        if (patientResult.getCode() == 20040) {
            return new Result(Code.ADD_ERROR, null, "新增病历失败！");
        }
        Object patient = patientResult.getData();
        if (Objects.isNull(patient)) {
            return new Result(Code.ADD_ERROR, null, "该患者不存在！");
        }

        medicalRecord.setVisitDate(LocalDateTime.now());
        boolean saved = medicalRecordService.save(medicalRecord);
        if (saved) {
            return new Result(Code.ADD_SUCCESS, null, "新增病历成功！");
        } else {
            return new Result(Code.ADD_ERROR, null, "新增病历失败！");
        }
    }

    /**
     * 删除医疗记录
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deleteMedicalRecord(@PathVariable Long id) {
        MedicalRecord existingMedicalRecord = medicalRecordService.getById(id);
        if (Objects.isNull(existingMedicalRecord)) {
            return new Result(Code.DELETE_ERROR, null, "该病历不存在！");
        }

        if (20020 == prescriptionClient.deleteByMedicalRecordId(id).getCode()) {
            return new Result(Code.DELETE_ERROR, null, "删除病历失败！");
        }

        boolean removed = medicalRecordService.removeById(id);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除病历成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除病历失败！");
        }
    }

    /**
     * 修改医疗记录
     *
     * @param medicalRecord
     * @return
     */
    @PutMapping
    public Result updateMedicalRecord(@RequestBody MedicalRecord medicalRecord) {
        boolean updated = medicalRecordService.updateById(medicalRecord);
        if (updated) {
            return new Result(Code.UPDATE_SUCCESS, null, "更新病历成功！");
        } else {
            return new Result(Code.UPDATE_ERROR, null, "更新病历失败！");
        }
    }

    /**
     * 根据ID查询医疗记录
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result getMedicalRecord(@PathVariable Long id) {
        MedicalRecord medicalRecord = medicalRecordService.getById(id);
        if (Objects.nonNull(medicalRecord)) {
            return new Result(Code.SELECT_SUCCESS, medicalRecord, "查询病历成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询病历失败！");
        }
    }

    /**
     * 查询医疗记录
     *
     * @param medicalRecord
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/{current}/{orderType}/{field}")
    public Result searchMedicalRecords(@RequestBody MedicalRecord medicalRecord,
                                       @PathVariable("current") Integer current,
                                       @PathVariable("orderType") String orderType,
                                       @PathVariable("field") String field) {
        List<MedicalRecord> medicalRecords = medicalRecordService.searchMedicalRecords(medicalRecord, current, orderType, field);
        if (null != medicalRecords && !medicalRecords.isEmpty()) {
            for (MedicalRecord _medicalRecord : medicalRecords) {
                com.shynnness.feign.pojo.Result patientResult = patientClient.getPatient(_medicalRecord.getPatientId());
                if (patientResult.getCode() == 20040) {
                    return new Result(Code.SELECT_ERROR, null, "查询病历失败！");
                }
                _medicalRecord.setPatient(patientResult.getData());
            }
            return new Result(Code.SELECT_SUCCESS, medicalRecords, "查询病历成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询病历失败！");
        }
    }

    /**
     * 统计医疗记录总数
     *
     * @param medicalRecord
     * @return
     */
    @PostMapping("/count")
    public Result countMedicalRecords(@RequestBody MedicalRecord medicalRecord) {
        Integer total = medicalRecordService.countMedicalRecords(medicalRecord);
        if (Objects.nonNull(total)) {
            return new Result(Code.SELECT_SUCCESS, total, "查询医疗记录总数成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询医疗记录总数失败！");
        }
    }

    /**
     * 以PDF格式导出医疗记录
     *
     * @param id
     * @return
     */
    @GetMapping("/exportPdf/{id}")
    public ResponseEntity<byte[]> exportMedicalRecordPdf(@PathVariable Long id) {
        // 根据ID查询病历
        MedicalRecord medicalRecord = medicalRecordService.getById(id);
        if (medicalRecord == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        // 根据ID查询患者
        com.shynnness.feign.pojo.Result patientResult = patientClient.getPatient(medicalRecord.getPatientId());
        if (patientResult.getCode() == 20040) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        LinkedHashMap patientMap = (LinkedHashMap) patientResult.getData();
        // URL编码
        String encodedFilename = null;
        try {
            String filename = medicalRecord.getId() + "-" + patientMap.get("name").toString() + ".pdf";
            encodedFilename = URLEncoder.encode(filename, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // 根据医疗记录ID查询处方
        com.shynnness.feign.pojo.Result prescriptionResult = prescriptionClient.getByMedicalRecordId(medicalRecord.getId());
        ObjectMapper objectMapper = new ObjectMapper();
        List<Prescription> prescriptions = objectMapper.convertValue(prescriptionResult.getData(), new TypeReference<List<Prescription>>() {
        });

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfDocument pdfDocument = new PdfDocument(new PdfWriter(baos));
        Document document = new Document(pdfDocument);

        try {
            PdfFont font = PdfFontFactory.createFont("/fonts/simsun.ttf", PdfEncodings.IDENTITY_H, true);

            Paragraph header = new Paragraph("电子病历").setFont(font).setBold();
            header.setTextAlignment(TextAlignment.CENTER);
            header.setFontSize(16);

            Table patientTable = new Table(new float[]{1, 3});
            patientTable.setWidth(UnitValue.createPercentValue(100));
            patientTable.addCell(new Cell().add(new Paragraph("患者编号").setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph(String.valueOf(patientMap.get("id"))).setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph("姓名").setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph(String.valueOf(patientMap.get("name"))).setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph("性别").setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph(String.valueOf(patientMap.get("gender"))).setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph("出生日期").setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph(String.valueOf(patientMap.get("birthDate"))).setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph("血型").setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph(String.valueOf(patientMap.get("bloodType"))).setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph("药物过敏信息").setFont(font)));
            patientTable.addCell(new Cell().add(new Paragraph(String.valueOf(patientMap.get("allergies"))).setFont(font)));

            Table medicalRecordTable = new Table(new float[]{1, 3});
            medicalRecordTable.setWidth(UnitValue.createPercentValue(100));
            medicalRecordTable.addCell(new Cell().add(new Paragraph("病历编号").setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph(String.valueOf(medicalRecord.getId())).setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph("访问日期").setFont(font)));
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String formattedDate = medicalRecord.getVisitDate().format(formatter);
            medicalRecordTable.addCell(new Cell().add(new Paragraph(formattedDate).setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph("科室").setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph(String.valueOf(medicalRecord.getDepartment())).setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph("症状").setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph(String.valueOf(medicalRecord.getSymptoms())).setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph("诊断").setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph(String.valueOf(medicalRecord.getDiagnosis())).setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph("治疗计划").setFont(font)));
            medicalRecordTable.addCell(new Cell().add(new Paragraph(String.valueOf(medicalRecord.getTreatmentPlan())).setFont(font)));

            document.add(header);
            document.add(new Paragraph("患者信息").setFont(font).setBold());
            document.add(patientTable);
            document.add(new Paragraph("病历").setFont(font).setBold());
            document.add(medicalRecordTable);

            document.add(new Paragraph("处方").setFont(font).setBold());
            Table prescriptionTable = null;
            if (null != prescriptions && !prescriptions.isEmpty()) {
                for (Prescription prescription : prescriptions) {
                    prescriptionTable = new Table((new float[]{1, 3}));
                    prescriptionTable.addCell(new Cell().add(new Paragraph("处方编号").setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph(String.valueOf(prescription.getId())).setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph("开具处方的医生姓名").setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph(String.valueOf(prescription.getDoctor().getName())).setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph("药物编号").setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph(String.valueOf(prescription.getMedicationId())).setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph("药物名称").setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph(String.valueOf(prescription.getMedication().getName())).setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph("用量").setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph(String.valueOf(prescription.getDosage())).setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph("用药方式").setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph(String.valueOf(prescription.getAdministration())).setFont(font)));
                    prescriptionTable.addCell(new Cell().add(new Paragraph("状态").setFont(font)));
                    String status = null;
                    switch (prescription.getStatus()) {
                        case 0:
                            status = "新处方";
                            break;
                        case 1:
                            status = "已批准";
                            break;
                        case 2:
                            status = "已拒绝";
                            break;
                    }
                    prescriptionTable.addCell(new Cell().add(new Paragraph(String.valueOf(status))).setFont(font));
                    prescriptionTable.addCell(new Cell().add(new Paragraph("电子签名").setFont(font)));
                    ImageData imageData = ImageDataFactory.create(prescription.getDoctorSignature());
                    Image image = new Image(imageData);
                    image.setHeight(50);
                    prescriptionTable.addCell(new Cell().add(image));
                    prescriptionTable.addCell(new Cell().add(new Paragraph(" ")).setHeight(10));
                    prescriptionTable.addCell(new Cell().add(new Paragraph(" ")).setHeight(10));
                    document.add(prescriptionTable);
                }
            } else {
                prescriptionTable = new Table(1);
                prescriptionTable.setWidth(UnitValue.createPercentValue(100));
                prescriptionTable.addCell(new Cell().add(new Paragraph("暂无处方！").setFont(font)));
                document.add(prescriptionTable);
            }

            document.close();
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        byte[] pdfContent = baos.toByteArray();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("attachment", encodedFilename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return new ResponseEntity<>(pdfContent, headers, HttpStatus.OK);
    }
}
