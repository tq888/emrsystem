package com.shynnness.medicalRecord.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shynnness.medicalRecord.pojo.NursingRecord;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface NursingRecordMapper extends BaseMapper<NursingRecord> {
}
