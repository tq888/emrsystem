package com.shynnness.medicalRecord.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shynnness.medicalRecord.pojo.MedicalRecord;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MedicalRecordMapper extends BaseMapper<MedicalRecord> {
}
