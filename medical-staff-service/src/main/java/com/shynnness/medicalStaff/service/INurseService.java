package com.shynnness.medicalStaff.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shynnness.medicalStaff.pojo.LoginRequest;
import com.shynnness.medicalStaff.pojo.Nurse;

import java.util.List;

public interface INurseService extends IService<Nurse> {

    /**
     * 检查电话是否存在
     *
     * @param phoneNumber
     * @return
     */
    boolean checkPhoneNumberExists(String phoneNumber);

    /**
     * 护士登录
     *
     * @param loginRequest
     * @return
     */
    Nurse login(LoginRequest loginRequest);

    /**
     * 查询护士
     *
     * @param nurse
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    List<Nurse> searchNurses(Nurse nurse, Integer current, String orderType, String field);

    /**
     * 统计护士总数
     *
     * @param nurse
     * @return
     */
    Integer countNurses(Nurse nurse);
}
