package com.shynnness.medicalStaff.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shynnness.medicalStaff.pojo.Doctor;
import com.shynnness.medicalStaff.pojo.LoginRequest;

import java.util.List;

public interface IDoctorService extends IService<Doctor> {

    /**
     * 检查电话是否存在
     *
     * @param phoneNumber
     * @return
     */
    boolean checkPhoneNumberExists(String phoneNumber);

    /**
     * 医生登录
     *
     * @param loginRequest
     * @return
     */
    Doctor login(LoginRequest loginRequest);

    /**
     * 查询医生
     *
     * @param doctor
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    List<Doctor> searchDoctors(Doctor doctor, Integer current, String orderType, String field);

    /**
     * 统计医生总数
     *
     * @param doctor
     * @return
     */
    Integer countDoctors(Doctor doctor);
}
