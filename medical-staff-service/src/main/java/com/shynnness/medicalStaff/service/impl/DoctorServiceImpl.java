package com.shynnness.medicalStaff.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shynnness.medicalStaff.mapper.DoctorMapper;
import com.shynnness.medicalStaff.pojo.Doctor;
import com.shynnness.medicalStaff.pojo.LoginRequest;
import com.shynnness.medicalStaff.service.IDoctorService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class DoctorServiceImpl extends ServiceImpl<DoctorMapper, Doctor> implements IDoctorService {
    @Autowired
    private DoctorMapper doctorMapper;

    @Override
    public boolean checkPhoneNumberExists(String phoneNumber) {
        QueryWrapper<Doctor> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phoneNumber", phoneNumber);
        return doctorMapper.selectCount(queryWrapper) > 0;
    }

    @Override
    public Doctor login(LoginRequest loginRequest) {
        QueryWrapper<Doctor> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phoneNumber", loginRequest.getPhoneNumber());
        queryWrapper.eq("password", loginRequest.getPassword());
        return doctorMapper.selectOne(queryWrapper);

    }

    @Override
    public List<Doctor> searchDoctors(Doctor doctor, Integer current, String orderType, String field) {
        QueryWrapper<Doctor> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(doctor, orderType, field, queryWrapper);
        return doctorMapper.selectPage(new Page<>(current, 10), queryWrapper).getRecords();
    }

    @Override
    public Integer countDoctors(Doctor doctor) {
        QueryWrapper<Doctor> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(doctor, null, null, queryWrapper);
        return Math.toIntExact(doctorMapper.selectCount(queryWrapper));
    }

    private static void WrapperBuilder(Doctor doctor, String orderType, String field, QueryWrapper<Doctor> queryWrapper) {
        if (Objects.nonNull(doctor.getId())) {
            queryWrapper.eq("id", doctor.getId());
        }
        if (StringUtils.isNotEmpty(doctor.getName())) {
            queryWrapper.like("name", doctor.getName());
        }
        if (StringUtils.isNotEmpty(doctor.getSpecialization())) {
            queryWrapper.like("specialization", doctor.getSpecialization());
        }
        if (StringUtils.isNotEmpty(doctor.getDepartment())) {
            queryWrapper.like("department", doctor.getDepartment());
        }
        if (StringUtils.isNotEmpty(doctor.getPhoneNumber())) {
            queryWrapper.like("phoneNumber", doctor.getPhoneNumber());
        }
        if (StringUtils.isNotEmpty(orderType) && StringUtils.isNotEmpty(field)) {
            if ("asc".equals(orderType)) {
                queryWrapper.orderByAsc(field);
            } else if ("desc".equals(orderType)) {
                queryWrapper.orderByDesc(field);
            }
        }
    }
}
