package com.shynnness.medicalStaff.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shynnness.medicalStaff.mapper.NurseMapper;
import com.shynnness.medicalStaff.pojo.LoginRequest;
import com.shynnness.medicalStaff.pojo.Nurse;
import com.shynnness.medicalStaff.service.INurseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class NurseServiceImpl extends ServiceImpl<NurseMapper, Nurse> implements INurseService {
    @Autowired
    private NurseMapper nurseMapper;

    @Override
    public boolean checkPhoneNumberExists(String phoneNumber) {
        QueryWrapper<Nurse> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phoneNumber", phoneNumber);
        return nurseMapper.selectCount(queryWrapper) > 0;
    }

    @Override
    public Nurse login(LoginRequest loginRequest) {
        QueryWrapper<Nurse> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phoneNumber", loginRequest.getPhoneNumber());
        queryWrapper.eq("password", loginRequest.getPassword());
        return nurseMapper.selectOne(queryWrapper);
    }

    @Override
    public List<Nurse> searchNurses(Nurse nurse, Integer current, String orderType, String field) {
        QueryWrapper<Nurse> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(nurse, orderType, field, queryWrapper);
        return nurseMapper.selectPage(new Page<>(current, 10), queryWrapper).getRecords();
    }

    @Override
    public Integer countNurses(Nurse nurse) {
        QueryWrapper<Nurse> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(nurse, null, null, queryWrapper);
        return Math.toIntExact(nurseMapper.selectCount(queryWrapper));
    }

    private static void WrapperBuilder(Nurse nurse, String orderType, String field, QueryWrapper<Nurse> queryWrapper) {
        if (Objects.nonNull(nurse.getId())) {
            queryWrapper.like("id", nurse.getId());
        }
        if (StringUtils.isNotEmpty(nurse.getName())) {
            queryWrapper.like("name", nurse.getName());
        }
        if (StringUtils.isNotEmpty(nurse.getDepartment())) {
            queryWrapper.like("department", nurse.getDepartment());
        }
        if (StringUtils.isNotEmpty(nurse.getPhoneNumber())) {
            queryWrapper.like("phoneNumber", nurse.getPhoneNumber());
        }
        if (StringUtils.isNotEmpty(orderType) && StringUtils.isNotEmpty(field)) {
            if ("asc".equals(orderType)) {
                queryWrapper.orderByAsc(field);
            } else if ("desc".equals(orderType)) {
                queryWrapper.orderByDesc(field);
            }
        }
    }
}
