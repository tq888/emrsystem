package com.shynnness.medicalStaff.web;

import com.baomidou.mybatisplus.extension.api.R;
import com.shynnness.medicalStaff.pojo.Code;
import com.shynnness.medicalStaff.pojo.LoginRequest;
import com.shynnness.medicalStaff.pojo.Nurse;
import com.shynnness.medicalStaff.pojo.Result;
import com.shynnness.medicalStaff.service.INurseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/nurse")
public class NurseController {
    @Autowired
    private INurseService nurseService;

    /**
     * 新增护士
     *
     * @param nurse
     * @return
     */
    @PostMapping
    public Result addNurse(@RequestBody Nurse nurse) {
        boolean phoneNumberExists = nurseService.checkPhoneNumberExists(nurse.getPhoneNumber());
        if (phoneNumberExists) {
            return new Result(Code.ADD_ERROR, null, "该手机号码已被注册！");
        }
        boolean saved = nurseService.save(nurse);
        if (saved) {
            return new Result(Code.ADD_SUCCESS, null, "新增护士成功！");
        } else {
            return new Result(Code.ADD_ERROR, null, "新增护士失败！");
        }
    }

    /**
     * 删除护士
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deleteNurse(@PathVariable Long id) {
        boolean removed = nurseService.removeById(id);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除护士成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除护士失败！");
        }
    }

    /**
     * 批量删除护士
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/batch")
    public Result deleteNurses(@RequestBody List<Long> ids) {
        if (ids.isEmpty()) {
            return new Result(Code.DELETE_ERROR, null, "批量删除护士失败！");
        }
        boolean removed = nurseService.removeByIds(ids);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "批量删除护士成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "批量删除护士失败！");
        }
    }

    /**
     * 更新护士
     *
     * @param nurse
     * @return
     */
    @PutMapping
    public Result updateNurse(@RequestBody Nurse nurse) {
        Nurse existingNurse = nurseService.getById(nurse.getId());
        if (Objects.isNull(existingNurse)) {
            return new Result(Code.UPDATE_ERROR, null, "该护士不存在！");
        }

        if (!existingNurse.getPhoneNumber().equals(nurse.getPhoneNumber())) {
            if (nurseService.checkPhoneNumberExists(nurse.getPhoneNumber())) {
                return new Result(Code.UPDATE_ERROR, null, "该手机号码已被注册！");
            }
        }

        boolean updated = nurseService.updateById(nurse);
        if (updated) {
            return new Result(Code.UPDATE_SUCCESS, null, "更新护士信息成功！");
        } else {
            return new Result(Code.UPDATE_ERROR, null, "更新护士信息失败！");
        }
    }

    /**
     * 根据ID查询护士
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result getNurse(@PathVariable Long id) {
        Nurse nurse = nurseService.getById(id);
        if (Objects.nonNull(nurse)) {
            return new Result(Code.SELECT_SUCCESS, nurse, "查询护士信息成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询护士信息失败！");
        }
    }

    /**
     * 护士登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/login")
    public Result login(@RequestBody LoginRequest loginRequest) {
        Nurse nurse = nurseService.login(loginRequest);
        if (Objects.nonNull(nurse)) {
            return new Result(Code.SELECT_SUCCESS, nurse, "登录成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }
    }

    /**
     * 查询护士
     *
     * @param nurse
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/{current}/{orderType}/{field}")
    public Result searchNurses(@RequestBody Nurse nurse,
                               @PathVariable("current") Integer current,
                               @PathVariable("orderType") String orderType,
                               @PathVariable("field") String field) {
        List<Nurse> nurses = nurseService.searchNurses(nurse, current, orderType, field);
        if (null != nurses && !nurses.isEmpty()) {
            return new Result(Code.SELECT_SUCCESS, nurses, "查询护士信息成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询护士信息失败！");
        }
    }

    /**
     * 统计护士总数
     *
     * @param nurse
     * @return
     */
    @PostMapping("/count")
    public Result countNurses(@RequestBody Nurse nurse) {
        Integer total = nurseService.countNurses(nurse);
        if (Objects.nonNull(total)) {
            return new Result(Code.SELECT_SUCCESS, total, "查询护士总数成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询护士总数失败！");
        }
    }
}
