package com.shynnness.medicalStaff.web;

import com.shynnness.medicalStaff.pojo.Code;
import com.shynnness.medicalStaff.pojo.Doctor;
import com.shynnness.medicalStaff.pojo.LoginRequest;
import com.shynnness.medicalStaff.pojo.Result;
import com.shynnness.medicalStaff.service.IDoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/doctor")
public class DoctorController {
    @Autowired
    private IDoctorService doctorService;

    /**
     * 新增医生
     *
     * @param doctor
     * @return
     */
    @PostMapping
    public Result addDoctor(@RequestBody Doctor doctor) {
        boolean phoneNumberExists = doctorService.checkPhoneNumberExists(doctor.getPhoneNumber());
        if (phoneNumberExists) {
            return new Result(Code.ADD_ERROR, null, "该手机号码已被注册！");
        }
        boolean saved = doctorService.save(doctor);
        if (saved) {
            return new Result(Code.ADD_SUCCESS, null, "新增医生成功！");
        } else {
            return new Result(Code.ADD_ERROR, null, "新增医生失败！");
        }
    }

    /**
     * 删除医生
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deleteDoctor(@PathVariable Long id) {
        boolean removed = doctorService.removeById(id);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除医生成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除医生失败！");
        }
    }

    /**
     * 批量删除医生
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/batch")
    public Result deleteDoctors(@RequestBody List<Long> ids) {
        if (ids.isEmpty()) {
            return new Result(Code.DELETE_ERROR, null, "批量删除医生失败！");
        }
        boolean removed = doctorService.removeByIds(ids);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "批量删除医生成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "批量删除医生失败！");
        }
    }

    /**
     * 更新医生
     *
     * @param doctor
     * @return
     */
    @PutMapping
    public Result updateDoctor(@RequestBody Doctor doctor) {
        Doctor existingDoctor = doctorService.getById(doctor.getId());
        if (Objects.isNull(existingDoctor)) {
            return new Result(Code.UPDATE_ERROR, null, "该医生不存在！");
        }

        if (!existingDoctor.getPhoneNumber().equals(doctor.getPhoneNumber())) {
            if (doctorService.checkPhoneNumberExists(doctor.getPhoneNumber())) {
                return new Result(Code.UPDATE_ERROR, null, "该手机号码已被注册！");
            }
        }

        boolean updated = doctorService.updateById(doctor);
        if (updated) {
            return new Result(Code.UPDATE_SUCCESS, null, "更新医生信息成功！");
        } else {
            return new Result(Code.UPDATE_ERROR, null, "更新医生信息失败！");
        }
    }

    /**
     * 根据ID查询医生
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result getDoctor(@PathVariable Long id) {
        Doctor doctor = doctorService.getById(id);
        if (Objects.nonNull(doctor)) {
            return new Result(Code.SELECT_SUCCESS, doctor, "查询医生信息成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询医生信息失败！");
        }
    }

    /**
     * 医生登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/login")
    public Result login(@RequestBody LoginRequest loginRequest) {
        Doctor doctor = doctorService.login(loginRequest);
        if (Objects.nonNull(doctor)) {
            return new Result(Code.SELECT_SUCCESS, doctor, "登录成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }
    }

    /**
     * 查询医生
     *
     * @param doctor
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/{current}/{orderType}/{field}")
    public Result searchDoctors(@RequestBody Doctor doctor,
                                @PathVariable("current") Integer current,
                                @PathVariable("orderType") String orderType,
                                @PathVariable("field") String field) {
        List<Doctor> doctors = doctorService.searchDoctors(doctor, current, orderType, field);
        if (null != doctors && !doctors.isEmpty()) {
            return new Result(Code.SELECT_SUCCESS, doctors, "查询医生信息成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询医生信息失败！");
        }
    }

    /**
     * 统计医生总数
     *
     * @param doctor
     * @return
     */
    @PostMapping("/count")
    public Result countDoctors(@RequestBody Doctor doctor) {
        Integer total = doctorService.countDoctors(doctor);
        if (Objects.nonNull(total)) {
            return new Result(Code.SELECT_SUCCESS, total, "查询医生总数成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询医生总数失败！");
        }
    }
}
