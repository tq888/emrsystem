package com.shynnness.medicalStaff.pojo;

import lombok.Data;

@Data
public class LoginRequest {
    private String phoneNumber;
    private String password;
}
