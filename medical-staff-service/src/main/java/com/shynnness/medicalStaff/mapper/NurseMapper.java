package com.shynnness.medicalStaff.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shynnness.medicalStaff.pojo.Nurse;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface NurseMapper extends BaseMapper<Nurse> {
}
