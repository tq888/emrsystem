package com.shynnness.medicalStaff.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shynnness.medicalStaff.pojo.Doctor;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DoctorMapper extends BaseMapper<Doctor> {
}
