package com.shynnness.adminFrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminFrontendApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminFrontendApplication.class, args);
    }
}
