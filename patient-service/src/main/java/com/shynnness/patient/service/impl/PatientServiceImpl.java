package com.shynnness.patient.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shynnness.patient.mapper.PatientMapper;
import com.shynnness.patient.pojo.LoginRequest;
import com.shynnness.patient.pojo.Patient;
import com.shynnness.patient.service.IPatientService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class PatientServiceImpl extends ServiceImpl<PatientMapper, Patient> implements IPatientService {
    @Autowired
    private PatientMapper patientMapper;

    @Override
    public boolean checkPhoneNumberExists(String phoneNumber) {
        QueryWrapper<Patient> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phoneNumber", phoneNumber);
        return patientMapper.selectCount(queryWrapper) > 0;
    }

    @Override
    public Patient login(LoginRequest loginRequest) {
        QueryWrapper<Patient> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phoneNumber", loginRequest.getPhoneNumber());
        queryWrapper.eq("password", loginRequest.getPassword());
        return patientMapper.selectOne(queryWrapper);
    }

    @Override
    public List<Patient> searchPatients(Patient patient, Integer current, String orderType, String field) {
        QueryWrapper<Patient> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(patient, orderType, field, queryWrapper);
        return patientMapper.selectPage(new Page<>(current, 10), queryWrapper).getRecords();
    }

    @Override
    public Integer countPatients(Patient patient) {
        QueryWrapper<Patient> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(patient, null, null, queryWrapper);
        return Math.toIntExact(patientMapper.selectCount(queryWrapper));
    }

    private static void WrapperBuilder(Patient patient, String orderType, String field, QueryWrapper<Patient> queryWrapper) {
        if (Objects.nonNull(patient.getId())) {
            queryWrapper.eq("id", patient.getId());
        }
        if (StringUtils.isNotEmpty(patient.getName())) {
            queryWrapper.like("name", patient.getName());
        }
        if (StringUtils.isNotEmpty(patient.getGender())) {
            queryWrapper.eq("gender", patient.getGender());
        }
        if (Objects.nonNull(patient.getBirthDate())) {
            queryWrapper.eq("birthDate", patient.getBirthDate());
        }
        if (StringUtils.isNotEmpty(patient.getAddress())) {
            queryWrapper.like("address", patient.getAddress());
        }
        if (StringUtils.isNotEmpty(patient.getPhoneNumber())) {
            queryWrapper.like("phoneNumber", patient.getPhoneNumber());
        }
        if (StringUtils.isNotEmpty(orderType) && StringUtils.isNotEmpty(field)) {
            if ("asc".equals(orderType)) {
                queryWrapper.orderByAsc(field);
            } else if ("desc".equals(orderType)) {
                queryWrapper.orderByDesc(field);
            }
        }
    }
}
