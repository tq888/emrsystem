package com.shynnness.patient.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shynnness.patient.pojo.LoginRequest;
import com.shynnness.patient.pojo.Patient;

import java.util.List;

public interface IPatientService extends IService<Patient> {

    /**
     * 检查电话是否存在
     *
     * @param phoneNumber
     * @return
     */
    boolean checkPhoneNumberExists(String phoneNumber);

    /**
     * 患者登录
     *
     * @param loginRequest
     * @return
     */
    Patient login(LoginRequest loginRequest);

    /**
     * 查询患者
     *
     * @param patient
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    List<Patient> searchPatients(Patient patient, Integer current, String orderType, String field);

    /**
     * 统计患者总数
     *
     * @param patient
     * @return
     */
    Integer countPatients(Patient patient);
}
