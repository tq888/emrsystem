package com.shynnness.patient.pojo;

import lombok.Data;

@Data
public class Result {
    private Object data;
    private Integer code;
    private String message;

    public Result(Integer code, Object data, String message) {
        this.data = data;
        this.code = code;
        this.message = message;
    }

    public Result(Integer code, Object data) {
        this.data = data;
        this.code = code;
    }

    public Result() {

    }
}
