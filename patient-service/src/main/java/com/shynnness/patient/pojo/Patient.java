package com.shynnness.patient.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "Patient")
public class Patient {

    /**
     * 患者ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 患者姓名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 患者密码
     */
    @TableField(value = "password")
    private String password;

    /**
     * 性别
     */
    @TableField(value = "gender")
    private String gender;

    /**
     * 出生日期
     */
    @TableField(value = "birthDate")
    private LocalDate birthDate;

    /**
     * 地址
     */
    @TableField(value = "address")
    private String address;

    /**
     * 联系电话
     */
    @TableField(value = "phoneNumber")
    private String phoneNumber;

    /**
     * 电子邮件地址
     */
    @TableField(value = "email")
    private String email;

    /**
     * 紧急联系人
     */
    @TableField(value = "emergencyContact")
    private String emergencyContact;

    /**
     * 紧急联系人电话
     */
    @TableField(value = "emergencyContactPhone")
    private String emergencyContactPhone;

    /**
     * 保险信息
     */
    @TableField(value = "insuranceDetails")
    private String insuranceDetails;

    /**
     * 民族
     */
    @TableField(value = "ethnicity")
    private String ethnicity;

    /**
     * 国籍
     */
    @TableField(value = "nationality")
    private String nationality;

    /**
     * 血型
     */
    @TableField(value = "bloodType")
    private String bloodType;

    /**
     * 身份证号/护照号
     */
    @TableField(value = "identificationNumber")
    private String identificationNumber;

    /**
     * 药物过敏信息
     */
    @TableField(value = "allergies")
    private String allergies;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    @TableField(value = "deleted")
    private Integer deleted;
}
