package com.shynnness.patient.pojo;

public class Code {
    public static final Integer ADD_SUCCESS = 20011;
    public static final Integer DELETE_SUCCESS = 20021;
    public static final Integer UPDATE_SUCCESS = 20031;
    public static final Integer SELECT_SUCCESS = 20041;

    public static final Integer ADD_ERROR = 20010;
    public static final Integer DELETE_ERROR = 20020;
    public static final Integer UPDATE_ERROR = 20030;
    public static final Integer SELECT_ERROR = 20040;
}
