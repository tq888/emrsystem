package com.shynnness.patient.web;

import com.shynnness.patient.pojo.LoginRequest;
import com.shynnness.patient.pojo.Code;
import com.shynnness.patient.pojo.Patient;
import com.shynnness.patient.pojo.Result;
import com.shynnness.patient.service.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/patient")
public class PatientController {
    @Autowired
    private IPatientService patientService;

    /**
     * 新增患者
     *
     * @param patient
     * @return
     */
    @PostMapping
    public Result addPatient(@RequestBody Patient patient) {
        boolean phoneNumberExists = patientService.checkPhoneNumberExists(patient.getPhoneNumber());
        if (phoneNumberExists) {
            return new Result(Code.ADD_ERROR, "该手机号码已被注册！");
        }
        boolean saved = patientService.save(patient);
        if (saved) {
            return new Result(Code.ADD_SUCCESS, null, "新增患者成功！");
        } else {
            return new Result(Code.ADD_ERROR, null, "新增患者失败！");
        }
    }

    /**
     * 删除患者
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deletePatient(@PathVariable Long id) {
        boolean removed = patientService.removeById(id);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除患者成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除患者失败！");
        }
    }

    /**
     * 批量删除患者
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/batch")
    public Result deletePatients(@RequestBody List<Long> ids) {
        if (ids.isEmpty()) {
            return new Result(Code.DELETE_ERROR, null, "批量删除患者失败！");
        }
        boolean removed = patientService.removeByIds(ids);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "批量删除患者成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "批量删除患者失败！");
        }
    }

    /**
     * 更新患者
     *
     * @param patient
     * @return
     */
    @PutMapping
    public Result updatePatient(@RequestBody Patient patient) {
        Patient existingPatient = patientService.getById(patient.getId());
        if (Objects.isNull(existingPatient)) {
            return new Result(Code.UPDATE_ERROR, null, "该患者不存在！");
        }

        if (!existingPatient.getPhoneNumber().equals(patient.getPhoneNumber())) {
            if (patientService.checkPhoneNumberExists(patient.getPhoneNumber())) {
                return new Result(Code.UPDATE_ERROR, null, "该手机号码已被注册！");
            }
        }

        boolean updated = patientService.updateById(patient);
        if (updated) {
            return new Result(Code.UPDATE_SUCCESS, null, "更新患者信息成功！");
        } else {
            return new Result(Code.UPDATE_ERROR, null, "更新患者信息失败！");
        }
    }

    /**
     * 根据ID查询患者
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result getPatient(@PathVariable Long id) {
        Patient patient = patientService.getById(id);
        if (Objects.nonNull(patient)) {
            return new Result(Code.SELECT_SUCCESS, patient, "查询患者信息成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询患者信息失败！");
        }
    }

    /**
     * 患者登录
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("/login")
    public Result login(@RequestBody LoginRequest loginRequest) {
        Patient patient = patientService.login(loginRequest);
        if (Objects.nonNull(patient)) {
            return new Result(Code.SELECT_SUCCESS, patient, "登录成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "登录失败，电话或密码错误！");
        }
    }

    /**
     * 查询患者
     *
     * @param patient
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/{current}/{orderType}/{field}")
    public Result searchPatients(@RequestBody Patient patient,
                                 @PathVariable("current") Integer current,
                                 @PathVariable("orderType") String orderType,
                                 @PathVariable("field") String field) {
        List<Patient> patients = patientService.searchPatients(patient, current, orderType, field);
        if (null != patients && !patients.isEmpty()) {
            return new Result(Code.SELECT_SUCCESS, patients, "查询患者信息成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询患者信息失败！");
        }
    }

    /**
     * 统计患者总数
     *
     * @param patient
     * @return
     */
    @PostMapping("/count")
    public Result countPatients(@RequestBody Patient patient) {
        Integer total = patientService.countPatients(patient);
        if (Objects.nonNull(total)) {
            return new Result(Code.SELECT_SUCCESS, total, "查询患者总数成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询患者总数失败！");
        }
    }
}
