package com.shynnness.medication.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shynnness.medication.pojo.Medication;

import java.util.List;

public interface IMedicationService extends IService<Medication> {

    /**
     * 查询药物
     *
     * @param medication
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    List<Medication> searchMedications(Medication medication, Integer current, String orderType, String field);

    /**
     * 统计药物总数
     *
     * @param medication
     * @return
     */
    Integer countMedications(Medication medication);
}
