package com.shynnness.medication.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shynnness.medication.mapper.MedicationMapper;
import com.shynnness.medication.pojo.Medication;
import com.shynnness.medication.service.IMedicationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class MedicationServiceImpl extends ServiceImpl<MedicationMapper, Medication> implements IMedicationService {
    @Autowired
    private MedicationMapper medicationMapper;

    @Override
    public List<Medication> searchMedications(Medication medication, Integer current, String orderType, String field) {
        QueryWrapper<Medication> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(medication, orderType, field, queryWrapper);
        return medicationMapper.selectPage(new Page<>(current, 10), queryWrapper).getRecords();
    }

    @Override
    public Integer countMedications(Medication medication) {
        QueryWrapper<Medication> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(medication, null, null, queryWrapper);
        return Math.toIntExact(medicationMapper.selectCount(queryWrapper));
    }

    private static void WrapperBuilder(Medication medication, String orderType, String field, QueryWrapper<Medication> queryWrapper) {
        if (Objects.nonNull(medication.getId())) {
            queryWrapper.eq("id", medication.getId());
        }
        if (StringUtils.isNotEmpty(medication.getName())) {
            queryWrapper.like("name", medication.getName());
        }
        if (StringUtils.isNotEmpty(medication.getGenericName())) {
            queryWrapper.like("genericName", medication.getGenericName());
        }
        if (StringUtils.isNotEmpty(medication.getManufacturer())) {
            queryWrapper.like("manufacturer", medication.getManufacturer());
        }
        if (Objects.nonNull(medication.getForm())) {
            queryWrapper.eq("form", medication.getForm());
        }
        if (StringUtils.isNotEmpty(medication.getStrength())) {
            queryWrapper.like("strength", medication.getStrength());
        }
        if (StringUtils.isNotEmpty(medication.getAdministration())) {
            queryWrapper.like("administration", medication.getAdministration());
        }
        if (StringUtils.isNotEmpty(orderType) && StringUtils.isNotEmpty(field)) {
            if ("asc".equals(orderType)) {
                queryWrapper.orderByAsc(field);
            } else if ("desc".equals(orderType)) {
                queryWrapper.orderByDesc(field);
            }
        }
    }
}
