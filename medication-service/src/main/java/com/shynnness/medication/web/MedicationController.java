package com.shynnness.medication.web;

import com.shynnness.medication.pojo.Code;
import com.shynnness.medication.pojo.Medication;
import com.shynnness.medication.pojo.Result;
import com.shynnness.medication.service.IMedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/medication")
public class MedicationController {
    @Autowired
    private IMedicationService medicationService;

    /**
     * 新增药物
     *
     * @param medication
     * @return
     */
    @PostMapping
    public Result addMedication(@RequestBody Medication medication) {
        boolean saved = medicationService.save(medication);
        if (saved) {
            return new Result(Code.ADD_SUCCESS, null, "新增药物成功！");
        } else {
            return new Result(Code.ADD_ERROR, null, "新增药物失败！");
        }
    }

    /**
     * 删除药物
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deleteMedication(@PathVariable Long id) {
        boolean removed = medicationService.removeById(id);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除药物成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除药物失败！");
        }
    }

    /**
     * 批量删除药物
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/batch")
    public Result deleteMedications(@RequestBody List<Long> ids) {
        if (ids.isEmpty()) {
            return new Result(Code.DELETE_ERROR, null, "批量删除药物失败！");
        }
        boolean removed = medicationService.removeByIds(ids);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "批量删除药物成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "批量删除药物失败！");
        }
    }

    /**
     * 更新药物
     *
     * @param medication
     * @return
     */
    @PutMapping
    public Result updateMedication(@RequestBody Medication medication) {
        boolean updated = medicationService.updateById(medication);
        if (updated) {
            return new Result(Code.UPDATE_SUCCESS, null, "更新药物信息成功！");
        } else {
            return new Result(Code.UPDATE_ERROR, null, "更新药物信息失败！");
        }
    }

    /**
     * 根据ID查询药物
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result getMedication(@PathVariable Long id) {
        Medication medication = medicationService.getById(id);
        if (Objects.nonNull(medication)) {
            return new Result(Code.SELECT_SUCCESS, medication, "查询药物成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询药物失败！");
        }
    }

    /**
     * 查询所有药物
     *
     * @return
     */
    @GetMapping
    public Result getAllMedications() {
        try {
            List<Medication> medications = medicationService.list();
            return new Result(Code.SELECT_SUCCESS, medications, "查询药物成功！");
        } catch (Exception e) {
            return new Result(Code.SELECT_ERROR, null, "查询药物失败！");
        }
    }

    /**
     * 查询药物
     *
     * @param medication
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/{current}/{orderType}/{field}")
    public Result searchMedications(@RequestBody Medication medication,
                                    @PathVariable("current") Integer current,
                                    @PathVariable("orderType") String orderType,
                                    @PathVariable("field") String field) {
        List<Medication> medications = medicationService.searchMedications(medication, current, orderType, field);
        if (null != medications && !medications.isEmpty()) {
            return new Result(Code.SELECT_SUCCESS, medications, "查询药物成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询药物失败！");
        }
    }

    /**
     * 统计药物总数
     *
     * @param medication
     * @return
     */
    @PostMapping("/count")
    public Result countMedications(@RequestBody Medication medication) {
        Integer total = medicationService.countMedications(medication);
        if (Objects.nonNull(total)) {
            return new Result(Code.SELECT_SUCCESS, total, "查询药物总数成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询药物总数失败！");
        }
    }
}
