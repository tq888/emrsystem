package com.shynnness.medication.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shynnness.medication.pojo.Medication;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MedicationMapper extends BaseMapper<Medication> {
}
