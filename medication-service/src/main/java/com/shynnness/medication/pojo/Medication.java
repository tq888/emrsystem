package com.shynnness.medication.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "Medication")
public class Medication {

    /**
     * 药物ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 药物名称
     */
    @TableField(value = "name")
    private String name;

    /**
     * 通用名
     */
    @TableField(value = "genericName")
    private String genericName;

    /**
     * 生产厂家
     */
    @TableField(value = "manufacturer")
    private String manufacturer;

    /**
     * 剂型
     */
    @TableField(value = "form")
    private Integer form;

    /**
     * 浓度或剂量
     */
    @TableField(value = "strength")
    private String strength;

    /**
     * 用药方式
     */
    @TableField(value = "administration")
    private String administration;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    @TableField(value = "deleted")
    private Integer deleted;
}
