package com.shynnness.patientFrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatientFrontendApplication {
    public static void main(String[] args) {
        SpringApplication.run(PatientFrontendApplication.class, args);
    }
}
