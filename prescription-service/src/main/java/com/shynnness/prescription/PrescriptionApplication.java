package com.shynnness.prescription;

import com.shynnness.feign.client.*;
import com.shynnness.feign.config.DefaultFeignConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(clients = {
        AdminClient.class,
        MedicalRecordClient.class,
        MedicalStaffClient.class,
        MedicationClient.class,
        PatientClient.class,
        PrescriptionClient.class
},
        defaultConfiguration = DefaultFeignConfiguration.class)
public class PrescriptionApplication {
    public static void main(String[] args) {
        SpringApplication.run(PrescriptionApplication.class, args);
    }
}
