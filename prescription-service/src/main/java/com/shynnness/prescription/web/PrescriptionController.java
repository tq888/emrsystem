package com.shynnness.prescription.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.shynnness.feign.client.MedicalStaffClient;
import com.shynnness.feign.client.MedicationClient;
import com.shynnness.prescription.pojo.Code;
import com.shynnness.prescription.pojo.Prescription;
import com.shynnness.prescription.pojo.Result;
import com.shynnness.prescription.service.IPrescriptionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/prescription")
public class PrescriptionController {
    @Autowired
    private IPrescriptionService prescriptionService;
    @Autowired
    private MedicalStaffClient medicalStaffClient;
    @Autowired
    private MedicationClient medicationClient;

    /**
     * 新增处方
     *
     * @param prescription
     * @return
     */
    @PostMapping
    public Result addPrescription(@RequestBody Prescription prescription) {
        String doctorSignatureBase64 = prescription.getDoctorSignatureBase64();
        System.out.println(doctorSignatureBase64);
        if (StringUtils.isNotEmpty(doctorSignatureBase64)) {
            // 检查并移除数据URI方案的前缀（如果存在）
            String base64Encoded = doctorSignatureBase64.startsWith("data:image")
                    ? doctorSignatureBase64.substring(doctorSignatureBase64.indexOf(",") + 1)
                    : doctorSignatureBase64;
            try {
                // 解码Base64字符串
                byte[] decodedSignature = Base64.getDecoder().decode(base64Encoded);
                prescription.setDoctorSignature(decodedSignature);
            } catch (IllegalArgumentException e) {
                // Base64解码失败
                return new Result(Code.ADD_ERROR, null, "电子签名格式错误！");
            }
        }

        boolean saved = prescriptionService.save(prescription);
        if (saved) {
            return new Result(Code.ADD_SUCCESS, null, "新增处方成功！");
        } else {
            return new Result(Code.ADD_ERROR, null, "新增处方失败！");
        }
    }

    /**
     * 删除处方
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deletePrescription(@PathVariable Long id) {
        boolean removed = prescriptionService.removeById(id);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除处方成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除处方失败！");
        }
    }

    /**
     * 根据医疗记录ID删除处方
     *
     * @param medicalRecordId
     * @return
     */
    @DeleteMapping("/medicalRecordId/{medicalRecordId}")
    public Result deleteByMedicalRecordId(@PathVariable Long medicalRecordId) {
        List<Prescription> existingPrescriptions = prescriptionService.getByMedicalRecordId(medicalRecordId);
        if (existingPrescriptions.isEmpty()) {
            return new Result(Code.DELETE_SUCCESS, null, "该病历无处方！");
        }

        boolean removed = prescriptionService.deleteByMedicalRecordId(medicalRecordId);
        if (removed) {
            return new Result(Code.DELETE_SUCCESS, null, "删除处方成功！");
        } else {
            return new Result(Code.DELETE_ERROR, null, "删除处方失败！");
        }
    }

    /**
     * 更新处方
     *
     * @param prescription
     * @return
     */
    @PutMapping
    public Result updatePrescription(@RequestBody Prescription prescription) {
        String doctorSignatureBase64 = prescription.getDoctorSignatureBase64();
        if (StringUtils.isNotEmpty(doctorSignatureBase64)) {
            // 检查并移除数据URI方案的前缀（如果存在）
            String base64Encoded = doctorSignatureBase64.startsWith("data:image")
                    ? doctorSignatureBase64.substring(doctorSignatureBase64.indexOf(",") + 1)
                    : doctorSignatureBase64;
            try {
                // 解码Base64字符串
                byte[] decodedSignature = Base64.getDecoder().decode(base64Encoded);
                prescription.setDoctorSignature(decodedSignature);
            } catch (IllegalArgumentException e) {
                // Base64解码失败
                return new Result(Code.UPDATE_ERROR, null, "电子签名格式错误！");
            }
        }

        boolean updated = prescriptionService.updateById(prescription);
        if (updated) {
            return new Result(Code.UPDATE_SUCCESS, null, "更新处方信息成功！");
        } else {
            return new Result(Code.UPDATE_ERROR, null, "更新处方信息失败！");
        }
    }

    /**
     * 根据医疗记录查询处方
     *
     * @param medicalRecordId
     * @return
     */
    @GetMapping("/medicalRecord/{medicalRecordId}")
    public Result getByMedicalRecordId(@PathVariable Long medicalRecordId) {
        List<Prescription> prescriptions = prescriptionService.getByMedicalRecordId(medicalRecordId);
        if (null != prescriptions && !prescriptions.isEmpty()) {
            for (Prescription prescription : prescriptions) {
                com.shynnness.feign.pojo.Result doctorResult = medicalStaffClient.getDoctor(prescription.getDoctorId());
                if (doctorResult.getCode() == 20040) {
                    return new Result(Code.SELECT_ERROR, null, "查询处方信息失败！");
                }
                prescription.setDoctor(doctorResult.getData());

                com.shynnness.feign.pojo.Result medicationResult = medicationClient.getMedication(prescription.getMedicationId());
                if (medicationResult.getCode() == 20040) {
                    return new Result(Code.SELECT_ERROR, null, "查询处方信息失败！");
                }
                prescription.setMedication(medicationResult.getData());
            }
            return new Result(Code.SELECT_SUCCESS, prescriptions, "查询处方成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询处方失败！");
        }
    }

    /**
     * 根据ID查询处方
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result getPrescription(@PathVariable Long id) {
        Prescription prescription = prescriptionService.getById(id);
        if (Objects.nonNull(prescription)) {
            // 将二进制数据转换为Base64编码字符串
            if (prescription.getDoctorSignature() != null) {
                String doctorSignatureBase64 = "data:image/png;base64," + Base64.getEncoder().encodeToString(prescription.getDoctorSignature());
                prescription.setDoctorSignatureBase64(doctorSignatureBase64);
                prescription.setDoctorSignature(null);
            }

            com.shynnness.feign.pojo.Result doctorResult = medicalStaffClient.getDoctor(prescription.getDoctorId());
            if (doctorResult.getCode() == 20040) {
                return new Result(Code.SELECT_ERROR, null, "查询处方信息失败！");
            }
            prescription.setDoctor(doctorResult.getData());

            com.shynnness.feign.pojo.Result medicationResult = medicationClient.getMedication(prescription.getMedicationId());
            if (medicationResult.getCode() == 20040) {
                return new Result(Code.SELECT_ERROR, null, "查询处方信息失败！");
            }
            prescription.setMedication(medicationResult.getData());

            return new Result(Code.SELECT_SUCCESS, prescription, "查询处方信息成功！");
        } else {
            return new Result(Code.SELECT_ERROR, null, "查询处方信息失败！");
        }
    }


    /**
     * 查询处方
     *
     * @param prescription
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    @PostMapping("/{current}/{orderType}/{field}")
    public Result searchPrescriptions(@RequestBody Prescription prescription,
                                      @PathVariable("current") Integer current,
                                      @PathVariable("orderType") String orderType,
                                      @PathVariable("field") String field) {
        List<Prescription> prescriptions = prescriptionService.searchPrescriptions(prescription, current, orderType, field);
        if (null != prescriptions && !prescriptions.isEmpty()) {
            for (Prescription _prescription : prescriptions) {
                com.shynnness.feign.pojo.Result doctorResult = medicalStaffClient.getDoctor(_prescription.getDoctorId());
                if (doctorResult.getCode() == 20040) {
                    return new Result(Code.SELECT_ERROR, null, "查询处方信息失败！");
                }
                _prescription.setDoctor(doctorResult.getData());

                com.shynnness.feign.pojo.Result medicationResult = medicationClient.getMedication(_prescription.getMedicationId());
                if (medicationResult.getCode() == 20040) {
                    return new Result(Code.SELECT_ERROR, null, "查询处方信息失败！");
                }
                _prescription.setMedication(medicationResult.getData());
            }
        }
        /*if (null != prescriptions && !prescriptions.isEmpty()) {*/
        return new Result(Code.SELECT_SUCCESS, prescriptions, "查询处方信息成功！");
        /*} else {
            return new Result(Code.SELECT_ERROR, null, "查询处方信息失败！");
        }*/
    }

    /**
     * 统计处方总数
     *
     * @param prescription
     * @return
     */
    @PostMapping("/count")
    public Result countPrescriptions(@RequestBody Prescription prescription) {
        Integer total = prescriptionService.countPrescriptions(prescription);
        /*if (Objects.nonNull(total)) {*/
        return new Result(Code.SELECT_SUCCESS, total, "查询处方总数成功！");
        /*} else {
            return new Result(Code.SELECT_ERROR, null, "查询处方总数失败！");
        }*/
    }

    /**
     * 审核处方
     *
     * @param id
     * @param status
     * @return
     */
    @PutMapping("/review/{id}/{status}")
    public Result reviewPrescription(@PathVariable Long id, @PathVariable Integer status) {
        Prescription prescription = prescriptionService.getById(id);
        if (Objects.isNull(prescription)) {
            return new Result(Code.SELECT_ERROR, null, "该处方不存在！");
        }
        prescription.setStatus(status);
        boolean updated = prescriptionService.updateById(prescription);
        if (updated) {
            return new Result(Code.UPDATE_SUCCESS, null, "审核处方成功！");
        } else {
            return new Result(Code.UPDATE_ERROR, null, "审核处方失败！");
        }
    }
}
