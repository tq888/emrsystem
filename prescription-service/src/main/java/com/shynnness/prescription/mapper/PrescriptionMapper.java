package com.shynnness.prescription.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shynnness.prescription.pojo.Prescription;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PrescriptionMapper extends BaseMapper<Prescription> {
}
