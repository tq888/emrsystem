package com.shynnness.prescription.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "Prescription")
public class Prescription {

    /**
     * 处方ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 医疗记录ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "medicalRecordId")
    private Long medicalRecordId;

    /**
     * 开具处方的医生ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "doctorId")
    private Long doctorId;

    /**
     * 药物ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "medicationId")
    private Long medicationId;

    /**
     * 用量
     */
    @TableField(value = "dosage")
    private String dosage;

    /**
     * 用药方式
     */
    @TableField(value = "administration")
    private String administration;

    /**
     * 状态
     * （0为新处方，1为已批准，2为已拒绝）
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 医生电子签名
     */
    @TableField(value = "doctorSignature")
    private byte[] doctorSignature;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    @TableField(value = "deleted")
    private Integer deleted;

    @TableField(exist = false)
    private Object doctor;
    @TableField(exist = false)
    private Object medication;
    @TableField(exist = false)
    private String doctorSignatureBase64;
}
