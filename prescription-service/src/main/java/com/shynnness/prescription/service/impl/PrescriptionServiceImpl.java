package com.shynnness.prescription.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shynnness.prescription.mapper.PrescriptionMapper;
import com.shynnness.prescription.pojo.Prescription;
import com.shynnness.prescription.service.IPrescriptionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class PrescriptionServiceImpl extends ServiceImpl<PrescriptionMapper, Prescription> implements IPrescriptionService {
    @Autowired
    private PrescriptionMapper prescriptionMapper;

    @Override
    public boolean deleteByMedicalRecordId(Long medicalRecordId) {
        QueryWrapper<Prescription> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("medicalRecordId", medicalRecordId);
        return prescriptionMapper.delete(queryWrapper) > 0;
    }

    @Override
    public List<Prescription> getByMedicalRecordId(Long medicalRecordId) {
        QueryWrapper<Prescription> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("medicalRecordId", medicalRecordId);
        return prescriptionMapper.selectList(queryWrapper);
    }

    @Override
    public List<Prescription> searchPrescriptions(Prescription prescription, Integer current, String orderType, String field) {
        QueryWrapper<Prescription> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(prescription, orderType, field, queryWrapper);
        return prescriptionMapper.selectPage(new Page<>(current, 10), queryWrapper).getRecords();
    }

    @Override
    public Integer countPrescriptions(Prescription prescription) {
        QueryWrapper<Prescription> queryWrapper = new QueryWrapper<>();
        WrapperBuilder(prescription, null, null, queryWrapper);
        return Math.toIntExact(prescriptionMapper.selectCount(queryWrapper));
    }

    private static void WrapperBuilder(Prescription prescription, String orderType, String field, QueryWrapper<Prescription> queryWrapper) {
        if (Objects.nonNull(prescription.getId())) {
            queryWrapper.eq("id", prescription.getId());
        }
        if (Objects.nonNull(prescription.getMedicalRecordId())) {
            queryWrapper.eq("medicalRecordId", prescription.getMedicalRecordId());
        }
        if (Objects.nonNull(prescription.getDoctorId())) {
            queryWrapper.eq("doctorId", prescription.getDoctorId());
        }
        if (Objects.nonNull(prescription.getMedicationId())) {
            queryWrapper.eq("medicationId", prescription.getId());
        }
        if (StringUtils.isNotEmpty(prescription.getDosage())) {
            queryWrapper.eq("dosage", prescription.getDosage());
        }
        if (StringUtils.isNotEmpty(prescription.getAdministration())) {
            queryWrapper.eq("administration", prescription.getAdministration());
        }
        if (Objects.nonNull(prescription.getStatus())) {
            queryWrapper.eq("status", prescription.getStatus());
        }
        if (StringUtils.isNotEmpty(orderType) && StringUtils.isNotEmpty(field)) {
            if ("asc".equals(orderType)) {
                queryWrapper.orderByAsc(field);
            } else if ("desc".equals(orderType)) {
                queryWrapper.orderByDesc(field);
            }
        }
    }
}
