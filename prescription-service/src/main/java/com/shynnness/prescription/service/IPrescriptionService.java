package com.shynnness.prescription.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shynnness.prescription.pojo.Prescription;

import java.util.List;

public interface IPrescriptionService extends IService<Prescription> {

    /**
     * 根据医疗记录ID删除处方
     *
     * @param medicalRecordId
     * @return
     */
    boolean deleteByMedicalRecordId(Long medicalRecordId);

    /**
     * 根据医疗记录ID查询处方
     *
     * @param medicalRecordId
     * @return
     */
    List<Prescription> getByMedicalRecordId(Long medicalRecordId);

    /**
     * 查询处方
     *
     * @param prescription
     * @param current
     * @param orderType
     * @param field
     * @return
     */
    List<Prescription> searchPrescriptions(Prescription prescription, Integer current, String orderType, String field);

    /**
     * 统计处方总数
     *
     * @param prescription
     * @return
     */
    Integer countPrescriptions(Prescription prescription);
}
