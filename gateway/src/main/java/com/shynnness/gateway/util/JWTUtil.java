package com.shynnness.gateway.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Map;

public class JWTUtil {

    private static final String ADMIN_SECRET_KEY = "!Shynnness-admin"; // Admin密钥
    private static final String USER_SECRET_KEY = "!Shynnness-user";   // User密钥

    /**
     * 为管理员获取token
     *
     * @param map
     * @return
     */
    public static String getAdminToken(Map<String, String> map) {
        return generateToken(map, ADMIN_SECRET_KEY);
    }

    /**
     * 为用户获取token
     *
     * @param map
     * @return
     */
    public static String getUserToken(Map<String, String> map) {
        return generateToken(map, USER_SECRET_KEY);
    }

    /**
     * 生成token
     *
     * @param map
     * @param secret
     * @return
     */
    private static String generateToken(Map<String, String> map, String secret) {
        JWTCreator.Builder builder = JWT.create();

        map.forEach(builder::withClaim);

        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DATE, 3); // Token有效期为3天

        builder.withExpiresAt(instance.getTime());
        return builder.sign(Algorithm.HMAC256(secret));
    }

    /**
     * 验证token合法性
     *
     * @param token
     * @param isAdmin
     * @return
     */
    public static DecodedJWT verify(String token, boolean isAdmin) {
        String secret = isAdmin ? ADMIN_SECRET_KEY : USER_SECRET_KEY;
        return JWT.require(Algorithm.HMAC256(secret)).build().verify(token);
    }

    /**
     * 获取token信息
     *
     * @param token
     * @param isAdmin
     * @return
     */
    public static Map<String, Claim> getTokenInfo(String token, boolean isAdmin) {
        String secret = isAdmin ? ADMIN_SECRET_KEY : USER_SECRET_KEY;
        return JWT.require(Algorithm.HMAC256(secret)).build().verify(token).getClaims();
    }
}
