package com.shynnness.gateway.filter;

import com.shynnness.gateway.util.JWTUtil;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;


@Component
public class AuthorizeFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String path = request.getURI().getPath();

        List<String> allowPaths = Arrays.asList(
                "/authorization/admin/login",
                "/authorization/doctor/login",
                "/authorization/nurse/login",
                "/authorization/patient/login",
                "/authorization/patient/register"
        );

        // 如果请求路径在允许列表中，则继续链路请求
        if (allowPaths.contains(path)) {
            return chain.filter(exchange);
        }

        // 从请求头中获取token
        String token = request.getHeaders().getFirst("token");

        // 如果没有token或token为空，则返回错误信息
        if (token == null || token.isEmpty()) {
            return this.onError(exchange, "请求中缺少token", HttpStatus.UNAUTHORIZED);
        }

        try {
            // 根据请求路径确定用户类型
            boolean isAdminPath = path.startsWith("/admin/");
            boolean isUserPath = path.startsWith("/user/"); // 示例，区分不同用户

            if (isAdminPath) {
                // 验证管理员token
                JWTUtil.verify(token, true); // 假设第二参数为true表示管理员
            } else if (isUserPath) {
                // 验证普通用户token
                JWTUtil.verify(token, false); // 假设false表示普通用户
            }

            // 验证通过，继续链路请求
            return chain.filter(exchange);
        } catch (Exception e) {
            // 验证失败，返回错误信息
            return this.onError(exchange, "无效的token", HttpStatus.UNAUTHORIZED);
        }
    }

    private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(httpStatus);
        response.getHeaders().add("Location", "#");
        return exchange.getResponse().setComplete();
    }

    @Override
    public int getOrder() {
        return -1;
    }
}
